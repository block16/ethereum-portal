import { Directive, ElementRef, HostListener } from '@angular/core';
import {isNullOrUndefined} from "util";

@Directive({
  selector: '[ethereumAddress]'
})
export class EthereumAddressDirective {
  private with0xPrefix = new RegExp("^(0x){0,1}[a-fA-F0-9]{0,40}$", "g");
  private without0xPrefix: RegExp = new RegExp("^[a-fA-F0-9]{0,40}$", "g");
  private regularKeys = new RegExp("[a-zA-Z0-9]", "g");

  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight' ];

  constructor(private el: ElementRef) { }

  @HostListener('paste', ['$event'])
  onPaste(event: KeyboardEvent): void {
    this.processEvent(event);
  }

  @HostListener('keydown', [ '$event' ])
  onKeyDown(event: KeyboardEvent) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }

    if (!String(event.key).match(this.regularKeys)) {
      return;
    }

    this.processEvent(event.key);
  }

  private processEvent(data) {
    const children = [];
    for (let j = 0; j < this.el.nativeElement.children.length; j++) {
      children.push(this.el.nativeElement.children[j]);
    }

    // Lol @ BFS
    let foundElement = null;
    while (isNullOrUndefined(foundElement)) {
      const currentElement = children.pop();
      if (currentElement.nodeName === "INPUT") {
        foundElement = currentElement;
      } else {
        for (let j = 0; j < currentElement.children.length; j++) {
          children.push(currentElement.children[j]);
        }
      }

      // We didn't find it.
      if (children.length === 0) {
        console.log("Couldn't find input element for the EthereumAddress directive");
        return;
      }
    }

    let current: string = foundElement.value;

    if (isNullOrUndefined(current)) {
      current = "";
    }

    const next: string = current.concat(data);

    if (next && (next === "0" || next === "0x")) {
      return;
    }

    let regex: RegExp;
    if (next.startsWith("0x")) {
      regex = this.with0xPrefix;
    } else {
      regex = this.without0xPrefix;
    }

    if (next && !(next === "0" || next === "0x") && !String(next).match(regex)) {
      event.preventDefault();
    }
  }
}
