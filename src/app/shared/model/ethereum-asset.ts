import {BigNumber} from 'bignumber.js';
import {isNullOrUndefined} from "util";
import {Denominations} from "./denomination";

export class EthereumAsset {
  public prices: any;

  public name: string;
  public symbol: string;
  public amount: BigNumber;
  public decimalPlaces: number;
  public contractAddress: string;
  public gasLimit?: number;
  public hasSymbol?: boolean;

  constructor () {
    this.prices = {};
    Denominations.forEach(d => {
      this.prices[d.abbreviation] = 0.0;
    });
  }

  /**
   * Raw number of tokens
   * Used to convert from "normal", i.e. - 1.2 ETH form to amount without decimals
   * @param {number} amount
   * @returns {BigNumber}
   */
  public amountToRaw(amount: string | BigNumber): BigNumber {
    if (typeof amount === "string") {
      return new BigNumber(amount, 10).times(this.places());
    } else {
      return amount.times(this.places());
    }
  }

  /**
   * Helper function to get the calculated number of decimal places in regard to wei representation
   * @returns {BigNumber}
   */
  public places(): BigNumber {
    return new BigNumber(10).pow(this.decimalPlaces);
  }

  /**
   * Get calculated amount of tokens or ETH with regard to decimal places
   * @returns {BigNumber}
   */
  get calculatedAmount(): BigNumber {
    return this.amount.div(this.places());
  }

  /**
   * Convert from raw to normal form, 1 * 10^18 ETH -> 1.0 ETH
   * @param {BigNumber} value
   * @returns {BigNumber}
   */
  public calculateAmount(value: BigNumber): BigNumber {
    return value.div(this.places());
  }

  public hasBalance(): boolean {
    return this.calculatedAmount.gt(0);
  }

  public isETH(): boolean {
    return this.symbol === 'ETH';
  }

  public value(denomination: string): BigNumber {
    if (isNullOrUndefined(this.prices) || isNullOrUndefined(this.prices[denomination])) {
      return new BigNumber('0');
    }
    return new BigNumber(this.prices[denomination].toString()).times(this.calculatedAmount);
  }
}

export class EthereumAssetBuilder {
    public name: string;
    public symbol: string;
    public amount: BigNumber;
    public decimalPlaces: number;
    public contractAddress: string;
    public gasLimit?: number;
    public hasSymbol?: boolean;

    withName(name: string) {
      this.name = name;
      return this;
    }

    withSymbol(symbol: string) {
      this.symbol = symbol;
      return this;
    }

    withAmount(amount: BigNumber) {
      this.amount = amount;
      return this;
    }

    withDecimalPlaces(decimalPlaces: number) {
      this.decimalPlaces = decimalPlaces;
      return this;
    }

    withContractAddress(contractAddress: string) {
      this.contractAddress = contractAddress;
      return this;
    }

    withGasLimit(gasLimit: number) {
      this.gasLimit = gasLimit;
      return this;
    }

    withHasSymbol(hasSymbol: boolean) {
      this.hasSymbol = hasSymbol;
      return this;
    }

    build(): EthereumAsset {
      const asset: EthereumAsset = new EthereumAsset();

      asset.name = this.name;
      if (isNullOrUndefined(asset.name)) {
        throw Error('Asset name undefined');
      }

      asset.symbol = this.symbol;
      if (isNullOrUndefined(asset.symbol)) {
        throw Error('Asset sybmol undefined');
      }

      asset.amount = this.amount;
      if (isNullOrUndefined(asset.amount)) {
        throw Error('Asset amount undefined');
      }

      asset.decimalPlaces = this.decimalPlaces;
      if (isNullOrUndefined(asset.decimalPlaces)) {
        throw Error('Asset decimalPlaces undefined');
      }

      asset.contractAddress = this.contractAddress;
      if (isNullOrUndefined(asset.contractAddress) && this.symbol !== 'ETH') {
        throw Error('Asset contract address undefined undefined');
      } else {
        this.contractAddress = "";
      }

      asset.gasLimit = this.gasLimit;
      // Token txs are normally 30000 - 60000 gas
      if (isNullOrUndefined(asset.gasLimit)) {
        asset.gasLimit = 65000;
      }

      asset.hasSymbol = this.hasSymbol;
      if (isNullOrUndefined(this.hasSymbol)) {
        asset.hasSymbol = false;
      }

      return asset;
    }
  }
