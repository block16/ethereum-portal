
interface Denomination {
  abbreviation: string;
  symbol: string;
}

const Denominations: Denomination[] = [
  { abbreviation: "None", symbol: " " },
  { abbreviation: "USD",  symbol: "$" },
  { abbreviation: "EUR",  symbol: "€" },
  { abbreviation: "JPY",  symbol: "¥" }
];

export {
  Denomination,
  Denominations
};
