import {EthereumAsset, EthereumAssetBuilder} from "./ethereum-asset";
import {EthereumTransaction, EthereumTransactionBuilder} from "./ethereum-transaction";
import {BigNumber} from "bignumber.js";


describe('EthereumTransaction', () => {

  let eth: EthereumAsset;
  let sevenTeenDecimalToken: EthereumAsset;
  let zeroDecimalToken: EthereumAsset;

  let ethTransaction: EthereumTransaction;
  let seventeenDecimalTransaction: EthereumTransaction;
  let eighteenDecimalTransaction: EthereumTransaction;

  beforeAll(() => {
    BigNumber.config({EXPONENTIAL_AT: [-40, 40], DECIMAL_PLACES: 18 });
  });

  beforeEach(() => {
    eth = new EthereumAssetBuilder()
      .withAmount(new BigNumber('1500000000000000000'))
      .withGasLimit(21000)
      .withSymbol('ETH')
      .withName('Ethereum')
      .withDecimalPlaces(18)
      .build();

    sevenTeenDecimalToken = new EthereumAssetBuilder()
      .withAmount(new BigNumber('1500000000000000000'))
      .withGasLimit(21000)
      .withSymbol('T17')
      .withName('Token Seventeen Decimals')
      .withDecimalPlaces(17)
      .withContractAddress("0x00")
      .build();

    zeroDecimalToken = new EthereumAssetBuilder()
      .withAmount(new BigNumber('100'))
      .withGasLimit(21000)
      .withSymbol('T0')
      .withName('Token Zero Decimals')
      .withDecimalPlaces(0)
      .withContractAddress("0x00")
      .build();

    /* ethTransaction = new EthereumTransactionBuilder()
      .withGasPrice("8")
      .withGasLimit(eth.gasLimit)
      .withFromAddress()
      .withToAddress()
      .withNonce()
      .withValue()
      .withData("0x")
      .withAsset(eth)
      .build(); */
  });

  describe('valueToBN', () => {

  });

  describe('transactionFees', () => {

  });

  describe('transactionFeesGwei', () => {

  });
});
