export interface ThemeSource {
  name: string;
  primaryColor: string; // "text color"
  secondaryColor: string; // "background color"
  accentColor: string;
  processingColor: string;
  sendColor: string;
  receiveColor: string;
}
