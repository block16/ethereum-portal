import {Denomination} from "./denomination";

export interface UserPreferences {
  autoAuth: boolean;
  manualGas: boolean;
  showPreSigned: boolean;
  randomTheme: boolean;
  denomination: Denomination;
  provider: string;
  themeName: string;
}
