
export interface Theme {
  name: string;

  primaryColor: string;
  primaryColorRgb: any;
  primaryColorOp1: any;
  primaryColorOp2: any;
  primaryColorOp3: any;
  primaryColorOp4: any;

  secondaryColor: string;
  secondaryColorRgb: any;
  secondaryColorOp1: any;
  secondaryColorOp2: any;
  secondaryColorOp3: any;
  secondaryColorOp4: any;

  accentColor: string;
  accentColorRgb: any;
  accentColorOp1: any;
  accentColorOp2: any;
  accentColorOp3: any;
  accentColorOp4: any;

  newTxColor: string;
  newTxColorRgb: any;
  newTxColorOp1: any;
  newTxColorOp2: any;
  newTxColorOp3: any;
  newTxColorOp4: any;

  op1: number;
  op2: number;
  op3: number;
  op4: number;
  // op5: number;

  newTxOp1: number;
  newTxOp2: number;
  newTxOp3: number;
  newTxOp4: number;

  op1Style: any;
  op2Style: any;
  op3Style: any;
  op4Style: any;
  newTxOp1Style: any;
  newTxOp2Style: any;
  newTxOp3Style: any;
  newTxOp4Style: any;

  isDark: boolean;

  mainStyle: any;
  inverseMainStyle: any;
  accentMainStyle: any;

  primaryBackgroundStyle: any;
  secondaryBackgroundStyle: any;
  accentBackgroundStyle: any;
  newTxBackgroundStyle: any;

  overlayStyle: any;

  primaryTextStyle: any;
  secondaryTextStyle: any;
  accentTextStyle: any;
  newTxTextStyle: any;

  sectionHeaderStyle: any;

  buttonStyle: any;
  secondaryButtonStyle: any;
  accentButtonStyle: any;

  maxButtonOnStyle: any;
  maxButtonOffStyle: any;

  newTransactionTextStyle: any;

  primaryBorderStyle: any;
  primaryBorderStyleOp1: any;
  primaryBorderStyleOp2: any;
  primaryBorderStyleOp3: any;
  primaryBorderStyleOp4: any;

  secondaryBorderStyle: any;
  secondaryBorderStyleOp1: any;
  secondaryBorderStyleOp2: any;
  secondaryBorderStyleOp3: any;
  secondaryBorderStyleOp4: any;

  accentBorderStyle: any;
  accentBorderStyleOp1: any;
  accentBorderStyleOp2: any;
  accentBorderStyleOp3: any;
  accentBorderStyleOp4: any;

  newTxBorderStyle: any;
  newTxBorderStyleOp1: any;
  newTxBorderStyleOp2: any;
  newTxBorderStyleOp3: any;
  newTxBorderStyleOp4: any;

  toggleButtonOffStyle: any;
  toggleButtonOnStyle: any;

  toggleSwitchOffStyle: any;
  toggleSwitchOnStyle: any;

  textInputFocusStyle: any;
  textInputBlurStyle: any;
  newTxTextInputFocusStyle: any;
  newTxTextInputBlurStyle: any;

  selectOverlayStyle: any;

  selectArrowStyle: any;
  newTxSelectArrowStyle: any;

  selectStyle: any;

  processingBackgroundStyle: any;
  processingColor: any;
  sendColor: any;
  receiveColor: any;
}
