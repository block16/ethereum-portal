import {BigNumber} from "bignumber.js";
import {isNullOrUndefined} from "util";

export class TransactionInformation {
  public toAddress: string;
  public fromAddress: string;
  public status: string;
  public blockNumber: number;
  public kind: string;
  public assetName: string;
  public amount: BigNumber;
  public created: Date;
  public hash: string;

  confirmations(currentBlock: number): number {
    return (currentBlock - this.blockNumber);
  }

  static fromJsonRep(jsonRep: any): TransactionInformation {
    return new TransactionInformationBuilder()
      .withToAddress(jsonRep.toAddress)
      .withFromAddress(jsonRep.fromAddress)
      .withStatus(jsonRep.status)
      .withBlockNumber(jsonRep.blockNumber)
      .withKind(jsonRep.kind)
      .withAssetname(jsonRep.assetName)
      .withAmount(jsonRep.amount)
      .withCreated(jsonRep.created)
      .withHash(jsonRep.hash)
      .build();
  }

  toJsonRep(): any {
    return {
      'toAddress': this.toAddress,
      'fromAddress': this.fromAddress,
      'status': this.status,
      'blockNumber': this.blockNumber,
      'kind': this.kind,
      'assetName': this.assetName,
      'amount': this.amount.toString(),
      'created': this.created.getTime(),
      'hash': this.hash
    };
  }

  getClockTime(): string {
    const hh = this.created.getHours();
    const m = this.created.getMinutes();
    let h = hh;
    if (h >= 12) {
      h = hh - 12;
    }
    if (h === 0) {
      h = 12;
    }
    const min = m < 10 ? "0" + m : m;
    return h + ":" + min;
  }

  getAmOrPm() {
    const hh = this.created.getHours();
    let dd = "AM";
    if (hh >= 12) {
      dd = "PM";
    }
    return dd;
  }

  greaterThanHour(): boolean {
    const createdTime = this.created.getTime();
    const now = new Date().getTime();
    const diff = Math.abs(now - createdTime) / (1000 * 60 * 60);
    const hrs = Math.floor(diff);
    return hrs > 0;
  }
}

export class TransactionInformationBuilder {
  private toAddress: string;
  private fromAddress: string;
  private status: string;
  private blockNumber: number;
  private kind: string;
  private assetName: string;
  private amount: BigNumber;
  private created: Date;
  private hash: string;

  withStatus(status: string) {
    this.status = status;
    return this;
  }

  withToAddress(toAddress: string) {
    this.toAddress = toAddress.startsWith("0x") ? toAddress.substr(2) : toAddress;
    this.toAddress = this.toAddress.toLowerCase();
    return this;
  }

  withFromAddress(fromAddress: string) {
    this.fromAddress = fromAddress.startsWith("0x") ? fromAddress.substr(2) : fromAddress;
    this.fromAddress = this.fromAddress.toLowerCase();
    return this;
  }

  withBlockNumber(blockNumber: number) {
    this.blockNumber = blockNumber;
    return this;
  }

  withKind(kind: string) {
    this.kind = kind;
    return this;
  }

  withAssetname(assetName: string) {
    this.assetName = assetName;
    return this;
  }

  withAmount(amount: string | BigNumber) {
    if (typeof amount === "string") {
      this.amount = new BigNumber(amount);
    } else {
      this.amount = amount;
    }
    return this;
  }

  withCreated(created: number) {
    this.created = new Date(created);
    return this;
  }

  withHash(hash: string) {
    this.hash = hash.startsWith("0x") ? hash.substr(2) : hash;
    return this;
  }

  build(): TransactionInformation {
    const t = new TransactionInformation();

    t.toAddress = this.toAddress;

    t.fromAddress = this.fromAddress;

    t.status = this.status;
    if (isNullOrUndefined(t.status)) {
      throw Error('status was not defined');
    }

    t.blockNumber = this.blockNumber;

    t.kind = this.kind;
    if (isNullOrUndefined(t.kind)) {
      throw Error('kind was undefined');
    }

    t.assetName = this.assetName;

    t.amount = this.amount;
    if (isNullOrUndefined(t.amount)) {
      throw Error('amount was undefined');
    }

    t.created = this.created;

    t.hash = this.hash;

    return t;
  }
}
