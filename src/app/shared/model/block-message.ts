
interface BlockMessage {
  number: string;
  hash: string;
  timestamp: string;

  // More omitted here
  transactions: TransactionMessage[];
  receipts: ReceiptMessage[];
}

interface TransactionMessage {
  hash: string;
  nonce: string;
  blockHash: string;
  blockNumber: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: string;
  gasPrice: string;
  gas: string;
  input: string;
  creates: string;
  publicKey: string;
  raw: string;
  r: string;
  s: string;
  v: number;
}

interface ReceiptMessage {
  transactionHash: string;
  transactionIndex: string;
  blockHash: string;
  blockNumber: string;
  cumulativeGasUsed: string;
  gasUsed: string;
  contractAddress: string;
  root: string;
  status: string;
  from: string;
  to: string;
  logs: LogsMessage[];
  logsBloom: string;
}

interface LogsMessage {
  removed: boolean;
  logIndex: string;
  transactionIndex: string;
  transactionHash: string;
  blockHash: string;
  blockNumber: string;
  address: string;
  data: string;
  type: string;
  topics: string[];
}

export {
  BlockMessage,
  TransactionMessage,
  ReceiptMessage
};
