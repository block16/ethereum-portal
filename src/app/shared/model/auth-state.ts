export enum AuthState {
  none = "None",
  trezor = "Trezor",
  bitbox = "BitBox",
  metamask = "MetaMask",
  utcFile = "UTC File",
  privateKey = "Private Key",
  ledger = "Ledger",
}
