
export interface AssetInfo {
  name: string;
  symbol: string;
  decimals: number;
}

