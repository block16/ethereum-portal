import {isNullOrUndefined} from 'util';
import {EthereumAsset} from "./ethereum-asset";
import {BigNumber} from 'bignumber.js';

declare var ethereumjs;


export class EthereumTransaction {
  public gasLimit: string;
  public gasPrice: string;
  public fromAddress: string;
  public toAddress: string;
  public value: string;
  public nonce: string;
  public data?: string;

  public signature: string;
  public tokenToAddress;        // Used to display to address when there is a token being sent
  public tokenValue: BigNumber; // Used to display when there is a token value being sent
  public asset: EthereumAsset;
  public hash: string;

  constructor() { }

  public getUnsignedTx() {
    if (isNullOrUndefined(this.nonce)) {
      throw new Error("Nonce is not set");
    }
    return new ethereumjs.Tx({
      nonce: this.nonce,
      gasPrice: this.gasPrice,
      gasLimit: this.gasLimit,
      from: this.fromAddress,
      to: this.toAddress,
      value: this.value,
      data: this.data
    });
  }

  public txToString(): string {
    return JSON.stringify({
      nonce: this.nonce,
      gasPrice: this.gasPrice,
      gasLimit: this.gasLimit,
      from: this.fromAddress,
      to: this.toAddress,
      value: this.value,
      data: this.data
    });
  }

  public getUnsignedObject(): any {
    if (isNullOrUndefined(this.nonce)) {
      throw new Error("Nonce is not set");
    }
    return {
      nonce: this.nonce,
      gasPrice: this.gasPrice,
      gasLimit: this.gasLimit,
      from: this.fromAddress,
      to: this.toAddress,
      value: this.value,
      data: this.data
    };
  }

  public valueToBN(): BigNumber {
    if (this.asset.symbol === 'ETH') {
      return new BigNumber(this.value.substring(2), 16).div(this.asset.places());
    } else {
      // Tokenvalue here because regular value is ETH
      return this.tokenValue.div(this.asset.places());
    }
  }

  public transactionFees(): BigNumber {
    return new BigNumber(this.gasPrice.substring(2), 16)
      .times(new BigNumber(this.gasLimit.substring(2), 16))
      .div(new BigNumber(10).pow(18));
  }

  public transactionFeesGwei(): BigNumber {
    return this.transactionFees().times('1000000000');
  }
}

export class EthereumTransactionBuilder {
  public gasLimit: string;
  public gasPrice: string;
  public fromAddress: string;
  public toAddress: string;
  public value: string;
  public nonce: string;
  public data?: string;

  public signature: string;
  public tokenToAddress;        // Used to display to address when there is a token being sent
  public tokenValue: BigNumber; // Used to display when there is a token value being sent
  public asset: EthereumAsset;
  public hash: string;

  withGasLimit(gasLimit: string) {
    this.gasLimit = gasLimit;
    return this;
  }

  withGasPrice(gasPrice: string) {
    this.gasPrice = gasPrice;
    return this;
  }

  withFromAddress(fromAddress: string) {
    this.fromAddress = ethereumjs.Util.addHexPrefix(fromAddress);
    return this;
  }

  withToAddress(toAddress: string) {
    this.toAddress = ethereumjs.Util.addHexPrefix(toAddress);
    return this;
  }

  withValue(value: string) {
    this.value = value;
    return this;
  }

  withNonce(nonce: string) {
    this.nonce = nonce;
    return this;
  }

  withData(data: string) {
    this.data = data;
    return this;
  }

  withSignature(signature: string) {
    this.signature = signature;
    return this;
  }

  withTokenToAddress(tokenToAddress: string) {
    this.tokenToAddress = tokenToAddress;
    return this;
  }

  withTokenValue(tokenValue: BigNumber) {
    this.tokenValue = tokenValue;
    return this;
  }

  withAsset(asset: EthereumAsset) {
    this.asset = asset;
    return this;
  }

  withHash(hash: string) {
    this.hash = hash;
    return this;
  }

  build(): EthereumTransaction {
    const tx: EthereumTransaction = new EthereumTransaction();

    tx.gasLimit = this.gasLimit;
    if (isNullOrUndefined(tx.gasLimit)) {
      throw Error('gasLimit was undefined');
    }

    tx.gasPrice = this.gasPrice;
    if (isNullOrUndefined(tx.gasPrice)) {
      throw Error('gasPrice was undefined');
    }

    tx.fromAddress = this.fromAddress;
    if (isNullOrUndefined(tx.fromAddress)) {
      throw Error('fromAddress was undefined');
    }

    tx.toAddress = this.toAddress;
    if (isNullOrUndefined(tx.toAddress)) {
      throw Error('fromAddress was undefined');
    }

    tx.value = this.value;
    if (isNullOrUndefined(tx.toAddress)) {
      throw Error('toAddress was undefined');
    }

    tx.nonce = this.nonce;
    if (isNullOrUndefined(tx.nonce)) {
      throw Error('nonce was undefined');
    }

    tx.data = this.data;
    if (isNullOrUndefined(tx.data)) {
      throw Error('data was undefined');
    }

    tx.asset = this.asset;
    if (isNullOrUndefined(tx.asset)) {
      throw Error('Asset was undefined');
    }

    tx.signature = this.signature;

    // TODO: Maybe validation here?
    tx.tokenToAddress = this.tokenToAddress;        // Used to display to address when there is a token being sent

    tx.tokenValue = this.tokenValue;                // Used to display when there is a token value being sent
    if (!this.asset.isETH() && isNullOrUndefined(tx.tokenValue)) {
      throw Error('token transaction didn\'t have a token value');
    }

    tx.hash = this.hash;                            // This is null when a TX is first created

    return tx;
  }
}
