import {EthereumAsset, EthereumAssetBuilder} from "./ethereum-asset";
import {BigNumber} from "bignumber.js";


describe('EthereumAsset', () => {
  let eth: EthereumAsset;
  let sevenTeenDecimalToken: EthereumAsset;
  let zeroDecimalToken: EthereumAsset;

  beforeAll(() => {
    BigNumber.config({EXPONENTIAL_AT: [-40, 40], DECIMAL_PLACES: 18 });
  });

  beforeEach(() => {
    eth = new EthereumAssetBuilder()
      .withAmount(new BigNumber('1500000000000000000'))
      .withGasLimit(21000)
      .withSymbol('ETH')
      .withName('Ethereum')
      .withDecimalPlaces(18)
      .build();

    sevenTeenDecimalToken = new EthereumAssetBuilder()
      .withAmount(new BigNumber('1500000000000000000'))
      .withGasLimit(21000)
      .withSymbol('T17')
      .withName('Token Seventeen Decimals')
      .withDecimalPlaces(17)
      .withContractAddress("0x00")
      .build();

    zeroDecimalToken = new EthereumAssetBuilder()
      .withAmount(new BigNumber('100'))
      .withGasLimit(21000)
      .withSymbol('T0')
      .withName('Token Zero Decimals')
      .withDecimalPlaces(0)
      .withContractAddress("0x00")
      .build();
  });

  describe('amountToRaw', () => {
    it('should produce correct raw amount for eth', () => {
      let val = eth.amountToRaw('1.5');
      expect(val.toString()).toEqual('1500000000000000000');
      val = eth.amountToRaw(new BigNumber('1.5'));
      expect(val.toString()).toEqual('1500000000000000000');
      val = eth.amountToRaw('2');
      expect(val.toString()).toEqual('2000000000000000000');
      val = eth.amountToRaw(new BigNumber('2'));
      expect(val.toString()).toEqual('2000000000000000000');
    });

    it('should produce correct raw amount for seventeen decimal token', () => {
      let val = sevenTeenDecimalToken.amountToRaw('1.5');
      expect(val.toString()).toEqual('150000000000000000');
      val = sevenTeenDecimalToken.amountToRaw('2');
      expect(val.toString()).toEqual('200000000000000000');
    });

    it('should produce correct raw amount for zero decimal token', () => {
      let val = zeroDecimalToken.amountToRaw('1');
      expect(val.toString()).toEqual('1');
      val = zeroDecimalToken.amountToRaw('2');
      expect(val.toString()).toEqual('2');
    });

    // TODO: Bunk out for incorrect decimal calculation
  });

  describe('places', () => {
    it('should produce 10^18 places for eth', () => {
      const val = eth.places();
      expect(val.toString()).toEqual('1000000000000000000');
    });

    it('should produce 10^17 places for seventeen decimal token', () => {
      const val = sevenTeenDecimalToken.places();
      expect(val.toString()).toEqual('100000000000000000');
    });

    it('should produce 10^0 places for zero decimal token', () => {
      const val = zeroDecimalToken.places();
      expect(val.toString()).toEqual('1');
    });
  });

  describe('calculatedAmount', () => {
    describe('smallest unit of value', () => {
      it('should be 1 * 10^-18 for eighteen decimals', () => {
        eth.amount = new BigNumber('1');
        const val = eth.calculatedAmount;
        expect(val.toString()).toEqual(new BigNumber(".000000000000000001").toString());
      });

      it('should be 1 * 10^-17 for seventeen decimals ', () => {
        sevenTeenDecimalToken.amount = new BigNumber('1');
        const val = sevenTeenDecimalToken.calculatedAmount;
        expect(val.toString()).toEqual(new BigNumber(".00000000000000001").toString());
      });

      it('should be 1 * 10^0 for zero decimals', () => {
        zeroDecimalToken.amount = new BigNumber('1');
        const val = zeroDecimalToken.calculatedAmount;
        expect(val.toString()).toEqual(new BigNumber("1").toString());
      });
    });

    describe('general values for rounding errors', () => {
      it('should correct for eighteen decimals', () => {
        const decimals = new BigNumber('10').pow(18);
        for (let j = 0; j < 9; j++) {
          for (let i = 0; i < 36; i++) {
            const jval = j.toString();
            const amount = new BigNumber('10').pow(i).times(new BigNumber(j.toString())).plus(jval);
            eth.amount = amount;
            const val = eth.calculatedAmount;
            expect(val.toString()).toEqual(amount.div(decimals).toString());
          }
        }
      });

      it('should be correct for seventeen decimals ', () => {
        const decimals = new BigNumber('10').pow(17);
        for (let j = 0; j < 9; j++) {
          for (let i = 0; i < 36; i++) {
            const jval = j.toString();
            const amount = new BigNumber('10').pow(i).times(new BigNumber(j.toString())).plus(jval);
            sevenTeenDecimalToken.amount = amount;
            const val = sevenTeenDecimalToken.calculatedAmount;
            expect(val.toString()).toEqual(amount.div(decimals).toString());
          }
        }
      });

      it('should be correct for zero decimals', () => {
        const decimals = new BigNumber('1');
        for (let j = 0; j < 9; j++) {
          for (let i = 0; i < 36; i++) {
            const jval = j.toString();
            const amount = new BigNumber('10').pow(i).times(new BigNumber(jval)).plus(jval);
            zeroDecimalToken.amount = amount;
            const val = zeroDecimalToken.calculatedAmount;
            expect(val.toString()).toEqual(amount.div(decimals).toString());
          }
        }
      });
    });
  });

  describe('hasBalance', () => {
    it('should have balance', () => {
      let val = eth.hasBalance();
      expect(val).toEqual(true);
      val = sevenTeenDecimalToken.hasBalance();
      expect(val).toEqual(true);
      val = zeroDecimalToken.hasBalance();
      expect(val).toEqual(true);
    });

    it('should not have balance after setting to zero', () => {
      eth.amount = new BigNumber(0);
      const val = eth.hasBalance();
      expect(val).toEqual(false);
    });
  });
});
