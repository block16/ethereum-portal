import {AfterViewInit, Attribute, Component, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";

@Component({
  selector: 'app-themed-svg',
  templateUrl: './themed-svg.component.html',
  styleUrls: ['./themed-svg.component.scss']
})
export class ThemedSvgComponent implements OnInit, OnDestroy {

  @Input() src: string;
  @Input() srcDark: string;

  public theme: Theme;
  private themeSubscription: Subscription;

  constructor(
    @Attribute('svgColor') private svgColor: string,
    private themeService: ThemeService,
    private el: ElementRef
  ) {
    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
      this.updateSVG(this.theme);
    });
  }

  public updateSVG(theme) {
    // This is part of a hack-ey (but seemingly necessary) solution to dynamically change the color of
    // SVG images/icons to match the theme. It is used alongside ng-inline-svg, and the .primary-svg or .secondary-svg
    // class must be placed on the element with the [inlineSVG] directive, to indicate its color in relation to the theme.
    // I also manually edit the .svg files so that their height and width attributes are 100%, so they can be sized
    // by their parent element.
    if (this.svgColor === 'primary') {
      const primaryColorPaths = this.el.nativeElement.querySelectorAll('svg path');
      for (let i = 0; i < primaryColorPaths.length; i++) {
        // console.log(i);
        // console.log(primaryColorPaths[i]);
        primaryColorPaths[i].setAttribute('fill', theme.primaryColor);
        // primaryColorPaths[i].style.fill = primaryColor;
      }
      const primaryColorPolygons = this.el.nativeElement.querySelectorAll('svg polygon');
      for (let i = 0; i < primaryColorPolygons.length; i++) {
        // console.log(i);
        // console.log(primaryColorPolygons[i]);
        primaryColorPolygons[i].setAttribute('fill', theme.primaryColor);
        // primaryColorPolygons[i].style.fill = primaryColor;
      }
    } else if (this.svgColor === 'secondary') {
      const secondaryColorPaths = this.el.nativeElement.querySelectorAll('svg path');
      for (let i = 0; i < secondaryColorPaths.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPaths[i]);
        secondaryColorPaths[i].setAttribute('fill', theme.secondaryColor);
        // secondaryColorPaths[i].style.fill = secondaryColor;
      }
      const secondaryColorPolygons = this.el.nativeElement.querySelectorAll('svg polygon');
      for (let i = 0; i < secondaryColorPolygons.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPolygons[i]);
        secondaryColorPolygons[i].setAttribute('fill', theme.secondaryColor);
        // secondaryColorPolygons[i].style.fill = secondaryColor;
      }
    } else if (this.svgColor === 'accent') {
      const accentColorPaths = this.el.nativeElement.querySelectorAll('svg path');
      for (let i = 0; i < accentColorPaths.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPaths[i]);
        accentColorPaths[i].setAttribute('fill', theme.accentColor);
        // secondaryColorPaths[i].style.fill = secondaryColor;
      }
      const accentColorPolygons = this.el.nativeElement.querySelectorAll('svg polygon');
      for (let i = 0; i < accentColorPolygons.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPolygons[i]);
        accentColorPolygons[i].setAttribute('fill', theme.accentColor);
        // secondaryColorPolygons[i].style.fill = secondaryColor;
      }
    } else if (this.svgColor === 'new-tx') {
      const newTxColorPaths = this.el.nativeElement.querySelectorAll('svg path');
      for (let i = 0; i < newTxColorPaths.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPaths[i]);
        newTxColorPaths[i].setAttribute('fill', theme.newTxColor);
        // secondaryColorPaths[i].style.fill = secondaryColor;
      }
      const newTxColorPolygons = this.el.nativeElement.querySelectorAll('svg polygon');
      for (let i = 0; i < newTxColorPolygons.length; i++) {
        // console.log(i);
        // console.log(secondaryColorPolygons[i]);
        newTxColorPolygons[i].setAttribute('fill', theme.newTxColor);
        // secondaryColorPolygons[i].style.fill = secondaryColor;
      }
    }
  }

  ngOnInit() {
    // this.updateSVG(this.theme);
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

}
