import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../model/theme/theme";

@Component({
  selector: 'app-asset-display',
  templateUrl: './asset-display.component.html',
  styleUrls: ['./asset-display.component.scss'],
  host: {
    '[class.no-wrap]': 'true',
    '[class.linine-proportional]': 'true',
  }
})
export class AssetDisplayComponent {
  public nonGrayValue: string;
  public grayValue: string;

  public theme: Theme;
  private themeSubscription: Subscription;

  @Input()
  set amount(a: string) {
    const amount = a;
    const index = amount.indexOf(".");
    this.grayValue = "";
    if (index !== -1) {
      this.nonGrayValue = this.numberWithCommas(amount.substring(0, index + 3));
      this.grayValue = amount.substring(index + 3);
    } else {
      this.nonGrayValue = this.numberWithCommas(amount);
    }
  }
  constructor(private themeService: ThemeService) {
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
    this.grayValue = "";
    this.nonGrayValue = "";
  }

  public numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
