import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendFormComponent } from './send-form.component';
import {CoreModule} from "../../core/core.module";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {SharedModule} from "../shared.module";

describe('SendFormComponent', () => {
  let component: SendFormComponent;
  let fixture: ComponentFixture<SendFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  ],
      imports: [ CoreModule, SharedModule ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
