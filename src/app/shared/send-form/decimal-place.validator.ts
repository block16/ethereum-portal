import {FormGroup, ValidationErrors, ValidatorFn} from "@angular/forms";

/**
 * Validates that the user has the correct amount of decimal places for their token
 * @desc validates the number of decimal places a user entered for a particular token. This could
 * not be in a abstract control Validator because we need state from a couple different controls.
 */
export const DecimalPlaceValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
  let ret: any = null;
  // Convert to string and check the number of chars after the decimal place
  const value = form.controls['sendAmount'].value.toString();
  const asset = form.controls['sendAsset'].value;
  const decimalIndex = value.indexOf('.');
  if (decimalIndex !== -1) {
    const afterDecimal = value.substr(decimalIndex + 1);
    if (afterDecimal.length > asset.decimalPlaces) {
      ret = {'decimalPlaces': 'Incorrect number of decimal places.'};
    }
  }
  return ret;
};

