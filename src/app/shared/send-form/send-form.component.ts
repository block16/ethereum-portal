import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {DataShareService} from "../../core/data-share.service";
import {Web3Service} from "../../core/web3.service";
import {EthereumAsset} from "../model/ethereum-asset";
import {Subscription} from "rxjs/Subscription";
import {Block16Service} from "../../core/block16.service";
import {UserPreferences} from "../model/user-preferences";
import {UserPreferencesService} from "../../core/user-preferences.service";
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";
import {EthereumTransaction, EthereumTransactionBuilder} from "../model/ethereum-transaction";
import {isNullOrUndefined} from "util";
import {BigNumber} from 'bignumber.js';
import {NonNegative} from "../validators/non-negative.validator";
import {calculateGasPrice, toHexPadded, toWeiFromGweiHex} from "../utils";
import {DecimalPlaceValidator} from "./decimal-place.validator";
import {AddressValidator} from "../validators/address.validator";
import {ThemeService} from "../../core/theme.service";

declare var ethereumjs;

@Component({
  selector: 'app-send-form',
  templateUrl: './send-form.component.html',
  styleUrls: ['./send-form.component.scss']
})
export class SendFormComponent implements AfterViewInit, OnChanges, OnDestroy, OnInit {
  @Input() theme;
  @Input('transactionState') mode = 'initial';
  @Output() ethereumTransaction: EventEmitter<EthereumTransaction> = new EventEmitter<EthereumTransaction>(true);
  @Output() circleElement: EventEmitter<any> = new EventEmitter<any>(true);
  @ViewChild('absolute') _absolute = null;
  @ViewChild('content') _content: ElementRef = null;

  private userPrefSub: Subscription;
  public userPreferences: UserPreferences;

  public currentAddress = "";
  private currentAddressSubscription: Subscription;

  public currentAsset: EthereumAsset;
  private assetSubscription: Subscription;
  private sendAssetSubscription: Subscription;
  private gasPriceSubscription: Subscription;
  private gasLimitSubscription: Subscription;
  private dataLoadedSubscription: Subscription;
  private sendAmountSubscription: Subscription;
  public dataLoaded = false;

  public gasLimit = 21000;
  public gasPrice = 8;
  public assets: EthereumAsset[];

  public sendForm: FormGroup;
  public sendAmount = new BigNumber('0');
  public previousAmount = new BigNumber('0');
  public calculatedGasPrice = new BigNumber('0');

  public sendMax = false;

  private windowWidth: number;
  public initialCircleDiameter: number;
  public absoluteCircleStyle = {};
  public circleCalibrationInfo = {};

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = window.innerWidth;
    this.calibrateAbsoluteCircle();
  }

  constructor(
    private formBuilder: FormBuilder,
    private web3Service: Web3Service,
    private block16Service: Block16Service,
    private dataShareService: DataShareService,
    private assetService: Block16Service,
    private userPrefService: UserPreferencesService,
    private coreKeyManagerService: CoreKeyManagerService,
    private themeService: ThemeService
  ) {
    this.theme = this.themeService.getRandomTheme();
    this.themeService.theme.subscribe(t => this.theme = t);
  }

  // TODO: refactor this
  updateMaxValueForEth() {
    this.sendAmount = this.currentAsset.calculatedAmount.minus(this.calculatedGasPrice);
    if (this.sendAmount.gte(new BigNumber("0"))) {
      this.sendForm.controls['sendAmount'].setValue(this.sendAmount);
    } else {
      this.sendForm.controls['sendAmount'].setValue(new BigNumber("0"));
      // This is a temporary hack
      this.currentAsset = this.assets[0];
      this.updateMaxValueForEth();
      return;
    }
    return this.sendAmount;
  }

  calcGasPrice(): BigNumber {
    const gasLimitFormVal = this.sendForm.controls['gasLimit'].value;
    const gasPriceFormVal = this.sendForm.controls['gasPrice'].value;

    let gL;
    if (isNullOrUndefined(gasLimitFormVal) || gasLimitFormVal === '') {
      gL = this.gasLimit;
    } else {
      gL = this.sendForm.controls['gasLimit'].value;
    }

    let gP;
    if (isNullOrUndefined(gasPriceFormVal) || gasPriceFormVal === '') {
      gP = this.gasPrice;
    } else {
      gP = this.sendForm.controls['gasPrice'].value;
    }

    return calculateGasPrice(gL, gP);
  }

  /**
   * Activated when the user clicks the max button
   * @desc updates the 'sendAmount' {FormControl} with the correct value.
   */
  clickMaxButton() {
    this.sendMax = !this.sendMax;
    // if we are sending max with the toggle
    if (this.sendMax) {
      const val = this.sendForm.controls['sendAmount'].value;
      // Store previous so we can retrieve it if user deselects max
      this.previousAmount = isNullOrUndefined(val) || val === "" ? new BigNumber('0') : new BigNumber(val);
      this.sendAmount = this.currentAsset.calculatedAmount;
      if (this.currentAsset.isETH()) {
        this.updateMaxValueForEth();
      }
    } else {
      this.sendAmount = this.previousAmount;
      this.previousAmount = this.previousAmount = this.sendForm.controls['sendAmount'].value;
    }

    // Set the sendAmount form control with the previous value or the max value
    this.sendForm.controls['sendAmount'].setValue(this.sendAmount);
  }

  /**
   * Generates the transaction for the user after clicking send
   * @desc builds an Ethereum or Token transaction depending on user selection and resets the form.
   */
  generateTransaction() {
    const sendAsset = <EthereumAsset>this.sendForm.controls['sendAsset'].value;
    let rawAmount = toHexPadded(sendAsset.amountToRaw(this.sendForm.controls['sendAmount'].value.toString())); // parameter or hex
    let data = '0x';         // hex prefix if actual data
    let toAddress = ethereumjs.Util.toChecksumAddress(this.sendForm.controls['sendAddress'].value);
    let tokenToAddress = ''; // only if we are doing a token TX
    let tokenValue = new BigNumber('0');

    const gasPrice = toWeiFromGweiHex(this.sendForm.controls['gasPrice'].value.toString());
    const gasLimit = toHexPadded(this.sendForm.controls['gasLimit'].value);

    // If we are not sending eth, we need to use the contract's address
    if (sendAsset.symbol !== 'ETH') {
      if (isNullOrUndefined(sendAsset.contractAddress)) {
        throw new Error('Couldn\'t find address for contract, cannot send tokens');
      }
      // TODO: Assumption you have to send 0 value to transfer erc20 tokens?
      rawAmount = toHexPadded(0);
      tokenToAddress = toAddress;
      data = this.web3Service.erc20Transfer(
        toAddress,
        sendAsset.amountToRaw(this.sendForm.controls['sendAmount'].value.toString()).toString()
      );
      tokenValue = sendAsset.amountToRaw(this.sendForm.controls['sendAmount'].value.toString());
      toAddress = ethereumjs.Util.toChecksumAddress(sendAsset.contractAddress);
    }

    this.web3Service.getTransactionCount(this.coreKeyManagerService.currentAddress.value).subscribe((count: number) => {
      const nonce = toHexPadded(count);

      const transactionBuilder = new EthereumTransactionBuilder();
      transactionBuilder
        .withGasPrice(gasPrice)
        .withGasLimit(gasLimit)
        .withFromAddress(this.currentAddress)
        .withToAddress(toAddress)
        .withValue(rawAmount)
        .withNonce(nonce)
        .withData(data)
        .withTokenToAddress(tokenToAddress)
        .withAsset(sendAsset)
        .withTokenValue(tokenValue);

      const transaction = transactionBuilder.build();

      this.ethereumTransaction.emit(transaction);

      // Reset the form using previous values
      const oldGasPrice = this.sendForm.controls['gasPrice'].value;
      const oldGasLimit = this.sendForm.controls['gasLimit'].value;
      this.sendForm.reset({
        'sendAsset': sendAsset,
        'gasPrice': oldGasPrice,
        'gasLimit': oldGasLimit,
        'sendAmount': 0
      });
    });
  }

  absoluteCircleRadius() {
    const c = this._content.nativeElement;
    const size = .85;
    return Math.sqrt(
      Math.pow(
        Math.max(c.offsetWidth * size, c.offsetHeight * size) / 2, 2
      ) +
      Math.pow(
        Math.min(c.offsetWidth * size, c.offsetHeight * size) / 2, 2
      )
    );
  }

  getAbsoluteCircleOffset() {
    return this.dataShareService.isMobileSize(this.windowWidth) ? -20 : 44;
  }

  calibrateAbsoluteCircle() {
    const r = this.absoluteCircleRadius();
    const circle = this._absolute.nativeElement;
    const content = this._absolute.nativeElement.parentElement.parentElement;
    const contentOffsetLeft = content.offsetLeft;
    const contentOffsetTop = content.offsetTop + this.getAbsoluteCircleOffset();
    this.initialCircleDiameter = (r * 2) + 95;

    this.absoluteCircleStyle['width'] = this.initialCircleDiameter + 'px';
    this.absoluteCircleStyle['height'] = this.initialCircleDiameter + 'px';
    this.circleCalibrationInfo = {
      'width': this.initialCircleDiameter,
      'height': this.initialCircleDiameter,
      'totalOffsetLeft': contentOffsetLeft + circle.offsetLeft,
      'totalOffsetTop': contentOffsetTop + circle.offsetTop
    };
    this.circleElement.emit(this.circleCalibrationInfo);
  }

  ngAfterViewInit() {
    this.windowWidth = window.innerWidth;
    this.calibrateAbsoluteCircle();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.mode) {
      this.calibrateAbsoluteCircle();
    }
  }

  ngOnInit() {
    this.block16Service.rebuild();

    // Create the send form so we can add pragmatic validators
    this.sendForm = this.formBuilder.group({
      'sendAddress': ['', [Validators.required, AddressValidator]],
      'sendAmount': ['', [Validators.required, NonNegative]],
      'sendAsset': ['', [Validators.required]],
      'gasPrice': ['', [Validators.required, Validators.max(100), Validators.min(0)]],
      'gasLimit': ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    }, {
      validator: [DecimalPlaceValidator, this.sufficientBalanceValidator()] //
    });

    this.dataLoadedSubscription = this.dataShareService.dataLoaded.subscribe(dl => this.dataLoaded = dl);

    this.userPrefSub = this.userPrefService.userPreferences.subscribe(userPref => {
      this.userPreferences = userPref;
      this.calculatedGasPrice = this.calcGasPrice();
      if (this._absolute) {
        setTimeout(() => {
          this.calibrateAbsoluteCircle();
        }, 0);
      }
    });

    // Subscribe to the assets to make sure we're on the most current for the drop down
    this.assetSubscription = this.assetService.ethereumAssets.subscribe((assets: EthereumAsset[]) => {
      this.assets = assets.filter(a => (a.hasBalance() || a.isETH()));
      if (isNullOrUndefined(this.currentAsset)) {
        this.currentAsset = this.assets[0];
      } else {
        const updatedCurrent = this.assets.find(a => a.symbol === this.currentAsset.symbol);
        this.currentAsset = isNullOrUndefined(updatedCurrent) ? this.currentAsset[0] : updatedCurrent;
      }
      if (this._content) {
        this.calibrateAbsoluteCircle();
      }
    });

    // Subscribe to user changing the asset
    this.sendAssetSubscription = this.sendForm.controls['sendAsset'].valueChanges.subscribe((sendAsset: EthereumAsset) => {
      this.currentAsset = sendAsset;
      this.previousAmount = new BigNumber("0");

      // If they have send max selected, send the max of the current asset
      this.sendAmount = this.sendMax ? this.currentAsset.calculatedAmount : new BigNumber("0");
      if (this.currentAsset.isETH()) {
        this.sendAmount = this.sendMax ? this.updateMaxValueForEth() : new BigNumber("0");
      }

      this.sendForm.controls['sendAmount'].patchValue(this.sendAmount.toNumber());

      const currentAssetSymbol = this.currentAsset.symbol;
      const currentGasLimit = this.sendForm.controls['gasLimit'].value;

      // If they have manual gas, make sure not to change this if it's not ETH and not 21000 gas (because we're sending token)
      if (!this.userPreferences.manualGas || (currentAssetSymbol === 'ETH' && currentGasLimit === 21000)) {
        this.sendForm.patchValue({gasLimit: sendAsset.gasLimit});
      }
      this.calculatedGasPrice = this.calcGasPrice();
    });

    // If the current address changes, we want to update the UI
    this.currentAddressSubscription = this.coreKeyManagerService.currentAddress.subscribe(a => this.currentAddress = a);

    // Need to update the correct send amount if user changes the gasPrice with max selected
    this.gasPriceSubscription = this.sendForm.controls['gasPrice'].valueChanges.subscribe(() => {
      this.calculatedGasPrice = this.calcGasPrice();
      // Necessary here since Eth is the only asset affected by a gasPrice change
      if (this.sendMax && this.currentAsset.isETH()) {
        this.updateMaxValueForEth();
      }
    });

    // Check to see if the send amount corresponds with the number of decimal places
    this.sendAmountSubscription = this.sendForm.controls['sendAmount'].valueChanges.subscribe((val) => {
      // max button deselecting
      if (this.sendMax) {
        if (this.currentAsset.isETH()) {
          if (!(this.currentAsset.calculatedAmount.minus(this.calculatedGasPrice)).eq(new BigNumber(val.toString()).toString())) {
            this.sendMax = false;
          }
        } else if (!(new BigNumber(val.toString()).eq(this.currentAsset.calculatedAmount))) {
            this.sendMax = false;
        }
      }
    });

    // Need to update the correct send amount if user changes the gasLimit with max selected
    this.gasLimitSubscription = this.sendForm.controls['gasLimit'].valueChanges.subscribe(() => {
      this.calculatedGasPrice = this.calcGasPrice();
      // Necessary here since Eth is the only asset affected by a gasLimit change
      if (this.sendMax && this.currentAsset.isETH()) {
        this.updateMaxValueForEth();
      }
    });

    // Get the current gas price from the b16 api
    this.block16Service.getGasPrice().subscribe((gasPrice: BigNumber) => {
      if (!this.userPreferences.manualGas) {
        // convert to gwei
        this.gasPrice = gasPrice.div(new BigNumber("1000000000")).toNumber();
        this.sendForm.controls['gasPrice'].setValue(this.gasPrice);
      }
      this.calculatedGasPrice = this.calcGasPrice();
    });

    this.sendForm.patchValue({gasLimit: this.gasLimit, gasPrice: this.gasPrice, sendAmount: 0});

    // This hidden part of the form needs this to be valid if the user never interacts with it
    this.sendForm.controls['gasLimit'].updateValueAndValidity();
  }

  /**
   * Validates that a user has enough balance to send an ETH transaction
   */
  private sufficientBalanceValidator() {
    return (form: FormGroup): ValidationErrors | null => {
      let ret: any = null;
      if (isNullOrUndefined(this.assets)) {
        console.log("assets undefined");
        return ret;
      }

      if (isNullOrUndefined(this.currentAsset)) {
        console.log("current asset undefined");
        return ret;
      }

      if (isNullOrUndefined(form)) {
        console.log("form undefined");
        return ret;
      }

      const ethBalance = this.assets[0].calculatedAmount;

      const gasLimitFormVal = form.controls['gasLimit'].value;
      const gasPriceFormVal = form.controls['gasPrice'].value;

      let gL;
      if (isNullOrUndefined(gasLimitFormVal) || gasLimitFormVal === '') {
        gL = this.gasLimit;
      } else {
        gL = form.controls['gasLimit'].value;
      }

      let gP;
      if (isNullOrUndefined(gasPriceFormVal) || gasPriceFormVal === '') {
        gP = this.gasPrice;
      } else {
        gP = form.controls['gasPrice'].value;
      }

      const calcPrice = calculateGasPrice(gL, gP);
      const sendAmount = form.controls['sendAmount'].value;

      if (this.currentAsset.isETH()) {
        // value + gasPrice * gasLimit < ETH balance
        let sendValue = new BigNumber('0'); // In ETH right now
        if (!isNullOrUndefined(sendAmount) && sendAmount !== '') {
          sendValue = new BigNumber(sendAmount.toString()); // In ETH right now
        }
        const total = sendValue.plus(calcPrice);
        if (total.gt(ethBalance)) {
          ret = {'ethBalance': 'Insufficient balance for transaction.'};
        }
      } else {
        // make sure gasPrice * gasLimit < ETH balance
        if (calcPrice.gt(ethBalance)) {
          ret = {'ethBalance': 'Insufficient balance for transaction.'};
        }
      }
      return ret;
    };
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.sendForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    if (this.sendForm.invalid) {
      invalid.push(this.sendForm.invalid);
    }
    console.log(invalid);
    return invalid;
  }

  ngOnDestroy(): void {
    this.userPrefSub.unsubscribe();
    this.sendAssetSubscription.unsubscribe();
    this.currentAddressSubscription.unsubscribe();
    this.gasPriceSubscription.unsubscribe();
    this.gasLimitSubscription.unsubscribe();
    this.sendAmountSubscription.unsubscribe();
  }
}
