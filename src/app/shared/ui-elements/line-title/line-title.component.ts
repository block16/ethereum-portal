import {Attribute, Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../../core/theme.service";
import {Theme} from "../../model/theme/theme";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-line-title',
  templateUrl: './line-title.component.html',
  styleUrls: ['./line-title.component.scss'],
  host: {
    '[class.margin-escape]': 'this.isMarginEscaped',
  }
})
export class LineTitleComponent implements OnDestroy {

  public theme: Theme;
  private themeSubscription: Subscription;

  public lineStyle = {};
  public isMarginEscaped: boolean;

  constructor(@Attribute('title') public title: string = 'normal',
              @Attribute('color') public color: string = 'normal',
              @Attribute('margin-escape') public marginEscape: boolean | null,
              private themeService: ThemeService) {

    this.isMarginEscaped = !isNullOrUndefined(marginEscape);
    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
      if (color === 'primary') {
        this.lineStyle = this.theme.primaryBorderStyleOp2;
      } else if (color === 'secondary') {
        this.lineStyle = this.theme.secondaryBorderStyleOp2;
      } else if (color === 'accent') {
        this.lineStyle = this.theme.secondaryBorderStyleOp2;
      } else if (color === 'new-tx') {
        this.lineStyle = this.theme.newTxBorderStyleOp2;
      }
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
