import {Attribute, Component, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../../core/theme.service";
import {Theme} from "../../model/theme/theme";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  host: {
    '[class.small]': "this.isSmall",
    '[class.hover]': "this.hover",
    '[class.focus]': "this.focus",
    '[style.background]': `this.newTx ? this.theme.newTxColorOp1 : this.theme.primaryColorOp1`
  }
})
export class SelectComponent implements OnInit, OnDestroy {
  @Input() control: FormControl;
  @Input() options: any[];
  @Input() optionProperty: string;
  @Input() placeholder: string;
  @Input() initialValue: any;

  public hover = false;
  public focus = false;
  public theme: Theme;

  public isSmall = false;

  private themeSubscription: Subscription;

  constructor(
    @Attribute('small') public small: boolean | null,
    @Attribute('new-tx') public newTx: boolean | null,
    private themeService: ThemeService
  ) {
    this.isSmall = !isNullOrUndefined(small);
    this.newTx = !isNullOrUndefined(newTx);
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
  }

  ngOnInit() {
    if (!isNullOrUndefined(this.initialValue)) {
      this.control.setValue(this.initialValue, {onlySelf: true});
    }
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.hover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.hover = false;
  }
}
