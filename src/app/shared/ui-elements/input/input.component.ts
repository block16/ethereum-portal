import {AfterViewInit, Attribute, Component, ChangeDetectorRef, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import { FormControl } from '@angular/forms';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../../core/theme.service";
import {Theme} from "../../model/theme/theme";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, OnDestroy {
  @ViewChild('label') _label;
  @ViewChild('input') _input;
  @Input() control: FormControl;
  @Input() inputID: string;
  @Input() inputName: string;
  @Input() inputPlaceholder: string;
  @Input() type: string;
  @Input() min: number = null;
  @Input() max: number = null;
  @Input() step: number = null;

  public theme: Theme;
  public focus = false;
  public showLabel = false;

  public isIntegerOnly = false;
  public isNewTx = false;

  private themeSubscription: Subscription;

  constructor(
    @Attribute('new-tx') public newTx: boolean | null,
    @Attribute('integer-only') public integerOnly: boolean | null,
    @Attribute('autofocus') public autofocus: boolean | null,
    @Attribute('no-autocomplete') public noAutoComplete: string | null,
    private themeService: ThemeService
  ) {
    this.isIntegerOnly = !isNullOrUndefined(integerOnly);
    this.autofocus = (autofocus != null);
    // TODO: Make this an enum
    this.noAutoComplete = noAutoComplete != null ? 'off' : 'on';
    this.isNewTx = !isNullOrUndefined(newTx);
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }

  ngOnInit() {
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
    if (this.autofocus) {
      this._input.nativeElement.focus();
    }
  }
}