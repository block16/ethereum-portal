import {Attribute, Component, EventEmitter, Input, Output, OnDestroy} from '@angular/core';
import {ThemeService} from "../../../core/theme.service";
import {Subscription} from "rxjs/Subscription";
import {Theme} from "../../model/theme/theme";

@Component({
  selector: 'menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
  host: {
    '[class.static]': 'this.static'
  }
})
export class MenuButtonComponent implements OnDestroy {
  @Input() static = false;
  @Input() value = false;
  @Output() valueChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public theme: Theme;
  private themeSubscription: Subscription;

  constructor(@Attribute('main') public main: boolean | null,
              @Attribute('color') public color: string | null,
    private themeService: ThemeService) {
    this.color = color == null ? 'primary' : color;
    this.main = (main != null);
    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
    });
  }
  
  getBackgroundStyle(styleString) {
    if (styleString == 'primary') return this.theme.primaryBackgroundStyle;
    else if (styleString == 'secondary') return this.theme.secondaryBackgroundStyle;
    else if (styleString == 'new-tx') return this.theme.newTxBackgroundStyle;
  }

  click() {
    if (!this.static) {
      this.value = !this.value;
      this.valueChange.emit(this.value);
    }
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
