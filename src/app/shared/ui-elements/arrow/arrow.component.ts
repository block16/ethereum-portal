import { Component, Input, OnInit } from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../../core/theme.service";
import {Theme} from "../../model/theme/theme";

@Component({
  selector: 'app-arrow',
  templateUrl: './arrow.component.html',
  styleUrls: ['./arrow.component.scss'],
  host: {
  	'[class.from]': `this.direction == 'from'`,
    '[class.to]': `this.direction == 'to'`,
    '[class.up]': `this.direction == 'up'`
  }
})
export class ArrowComponent implements OnInit {
	
	@Input() direction: string;
	
  public theme: Theme;
  private themeSubscription: Subscription;
	
  constructor(private themeService: ThemeService) {
  	this.themeSubscription = this.themeService.theme.subscribe(theme => {
  	  this.theme = theme;
  	});
  }
  
  getArrowColor(direction) {
    if (direction == 'to') {
      return this.theme.sendColor;
    } else if (direction == 'from') {
      return this.theme.receiveColor;
    } else if (direction == 'up') {
      return this.theme.newTxColor;
    }
  }
  
  ngOnInit() {
  }

}
