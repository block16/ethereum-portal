import {Attribute, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {UserPreferencesService} from "../../core/user-preferences.service";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-token-list',
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.scss'],
  host: {
    '[class.desktop]': 'this.desktop',
  }
})
export class TokenListComponent implements OnDestroy {

  @Input() assets;

  private userPreferencesSubscription: Subscription;
  private themeSubscription: Subscription;
  public theme: Theme;
  public userPreferences;

  constructor(
    @Attribute('hide-eth') public hideETH: boolean | null,
    @Attribute('desktop') public desktop: boolean | null,
    private userPreferencesService: UserPreferencesService,
    private themeService: ThemeService
  ) {
    this.hideETH = (hideETH != null);
    this.desktop = (desktop != null);
    // Theme
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
    this.userPreferencesSubscription = this.userPreferencesService.userPreferences.subscribe(p => this.userPreferences = p);
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
    this.userPreferencesSubscription.unsubscribe();
  }

}
