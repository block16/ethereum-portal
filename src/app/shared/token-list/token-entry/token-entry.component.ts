import {Attribute, Component, DoCheck, Input, KeyValueDiffer, KeyValueDiffers, OnDestroy, OnInit} from '@angular/core';
import {UserPreferencesService} from "../../../core/user-preferences.service";
import {ThemeService} from "../../../core/theme.service";
import {Subscription} from "rxjs/Subscription";
import {checkTokenSymbol} from "../../utils";
import {EthereumAsset} from "../../model/ethereum-asset";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-token-entry',
  templateUrl: './token-entry.component.html',
  styleUrls: ['./token-entry.component.scss'],
  host: {
    '[class.eth]': "this.isETH"
  },
})
export class TokenEntryComponent implements OnInit, OnDestroy, DoCheck {
  public _token: EthereumAsset;
  @Input('token') set token(t: EthereumAsset) {
    this._token = t;
    this.updateChanges();
  }

  private tokenDiffer: KeyValueDiffer<string, any>;
  private themeSubscription: Subscription;
  private userPreferencesSubscription: Subscription;

  public theme;
  public userPreferences;

  public hasTokenIcon = false;
  public tokenSvgPath = "";
  public tokenAbbreviation = "";
  public tokenValue = "";
  public tokenAmount = "";

  constructor(
    @Attribute('eth') public isETH: boolean | null,
    private userPreferencesService: UserPreferencesService,
    private themeService: ThemeService,
    private differs: KeyValueDiffers
  ) {
    this.isETH = (isETH != null);
  }

  private tokenHasIcon(contractAddress) {
    return !isNullOrUndefined(checkTokenSymbol(contractAddress));
  }

  private getTokenAbbreviation(tokenTicker: string) {
    return tokenTicker.substr(0, 1).toUpperCase();
  }

  private tokenIconSVGPath(symbol) {
    return '/assets/img/icons/coins/' + symbol.toLowerCase() + '.svg';
  }

  ngOnInit(): void {
    this.tokenDiffer = this.differs.find(this._token).create();
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
    this.userPreferencesSubscription = this.userPreferencesService.userPreferences.subscribe(p => {
      this.userPreferences = p;
      this.updateChanges();
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.userPreferencesSubscription.unsubscribe();
  }

  ngDoCheck(): void {
    this.updateChanges();
  }

  private updateChanges() {
    if (!isNullOrUndefined(this._token) && !isNullOrUndefined(this.tokenDiffer)) {
      const changes = this.tokenDiffer.diff(this._token);
      if (changes) {
        this.hasTokenIcon = this.tokenHasIcon(this._token.contractAddress);
        if (this.hasTokenIcon) {
          this.tokenSvgPath = this.tokenIconSVGPath(this._token.symbol);
        }
        this.tokenAbbreviation = this.getTokenAbbreviation(this._token.symbol);
        this.tokenValue = "";
        this.tokenAmount = this._token.calculatedAmount.toString(10);
        const abbreviationDefined =
          !isNullOrUndefined(this.userPreferences) &&
          !isNullOrUndefined(this.userPreferences.denomination) &&
          !isNullOrUndefined(this.userPreferences.denomination.abbreviation) &&
          this.userPreferences.denomination.abbreviation !== "None";
        if (abbreviationDefined) {
          this.tokenValue = this._token.value(this.userPreferences.denomination.abbreviation).toFixed(2);
        }
      }
    }
  }
}
