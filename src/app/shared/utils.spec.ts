import { toHexPadded, toWeiFromGweiHex, toWeiFromGwei, calculateGasPrice } from './utils';
import {BigNumber} from "bignumber.js";

describe('Utils Functions', () => {

  describe('toHexPadded', () => {
    it('should produce right hex for 0', () => {
      const zeroHexNumber = toHexPadded(0);
      expect(zeroHexNumber).toBe('0x00');
      const zeroHexString = toHexPadded('0');
      expect(zeroHexString).toBe('0x00');
      const zeroHexBigNumber = toHexPadded(new BigNumber(0));
      expect(zeroHexBigNumber).toBe('0x00');
    });
    it('should produce right hex for 1', () => {
      const zeroHexNumber = toHexPadded(1);
      expect(zeroHexNumber).toBe('0x01');
      const zeroHexString = toHexPadded('1');
      expect(zeroHexString).toBe('0x01');
      const zeroHexBigNumber = toHexPadded(new BigNumber(1));
      expect(zeroHexBigNumber).toBe('0x01');
    });
    it('should produce right hex for 500001', () => {
      const zeroHexNumber = toHexPadded(500001);
      expect(zeroHexNumber).toBe('0x07A121'.toLowerCase());
      const zeroHexString = toHexPadded('500001');
      expect(zeroHexString).toBe('0x07A121'.toLowerCase());
      const zeroHexBigNumber = toHexPadded(new BigNumber(500001));
      expect(zeroHexBigNumber).toBe('0x07A121'.toLowerCase());
    });
  });

  describe('toWeiFromGweiHex', () => {
    it('should produce right value for 0 gwei', () => {
      const val = toWeiFromGweiHex('0');
      expect(val).toBe('0x00');
    });
    it('should produce right value for 1 gwei', () => {
      const val = toWeiFromGweiHex('1');
      expect(val).toBe('0x3B9ACA00'.toLowerCase());
    });
    it('should produce right value for 99 gwei', () => {
      const val = toWeiFromGweiHex('99');
      expect(val).toBe('0x170CDC1E00'.toLowerCase());
    });
  });

  describe('toWeiFromGwei', () => {
    it('should produce right value for 0 gwei', () => {
      const val = toWeiFromGwei('0');
      expect(val).toBe('0');
    });
    it('should produce right value for 1 gwei', () => {
      const val = toWeiFromGwei('1');
      expect(val).toBe('1000000000');
    });
    it('should produce right value for 99 gwei', () => {
      const val = toWeiFromGwei('99');
      expect(val).toBe('99000000000');
    });
  });

  describe('calculateGasPrice', () => {
    it('should produce right value for gasLimit 21000 and gasPrice 1.6 gwei', () => {
      let val = calculateGasPrice("21000", "1.6");
      expect(val.toString()).toEqual(new BigNumber('0.0000336').toString());
      val = calculateGasPrice(new BigNumber("21000"), new BigNumber("1.6"));
      expect(val.toString()).toEqual(new BigNumber('0.0000336').toString());
      val = calculateGasPrice(21000, 1.6);
      expect(val.toString()).toEqual(new BigNumber('0.0000336').toString());
    });

    it('should produce right value for gasLimit 21000 and gasPrice 99 gwei', () => {
      let val = calculateGasPrice("21000", "99");
      expect(val.toString()).toEqual(new BigNumber('0.002079').toString());
      val = calculateGasPrice(new BigNumber("21000"), new BigNumber("99"));
      expect(val.toString()).toEqual(new BigNumber('0.002079').toString());
      val = calculateGasPrice(21000, 99);
      expect(val.toString()).toEqual(new BigNumber('0.002079').toString());
    });

    it('should produce right value for gasLimit 21000 and gasPrice 1 gwei', () => {
      let val = calculateGasPrice("21000", "1");
      expect(val.toString()).toEqual(new BigNumber('0.000021').toString());
      val = calculateGasPrice(new BigNumber("21000"), new BigNumber("1"));
      expect(val.toString()).toEqual(new BigNumber('0.000021').toString());
      val = calculateGasPrice(21000, 1);
      expect(val.toString()).toEqual(new BigNumber('0.000021').toString());
    });

    it('should produce right value for gasLimit 21000 and gasPrice 100 gwei', () => {
      let val = calculateGasPrice("21000", "100");
      expect(val.toString()).toEqual(new BigNumber('0.0021').toString());
      val = calculateGasPrice(new BigNumber("21000"), new BigNumber("100"));
      expect(val.toString()).toEqual(new BigNumber('0.0021').toString());
      val = calculateGasPrice(21000, 100);
      expect(val.toString()).toEqual(new BigNumber('0.0021').toString());
    });

    it('should produce right value for gasLimit 65000 and gasPrice 100 gwei', () => {
      let val = calculateGasPrice("65000", "100");
      expect(val.toString()).toEqual(new BigNumber('0.0065').toString());
      val = calculateGasPrice(new BigNumber("65000"), new BigNumber("100"));
      expect(val.toString()).toEqual(new BigNumber('0.0065').toString());
      val = calculateGasPrice(65000, 100);
      expect(val.toString()).toEqual(new BigNumber('0.0065').toString());
    });
  });
});
