import { Attribute, Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";

@Component({
  selector: 'loading-icon',
  templateUrl: './loading-icon.component.html',
  styleUrls: ['./loading-icon.component.scss']
})
export class LoadingIconComponent implements OnInit {

	public theme: Theme;
	private themeSubscription: Subscription;
	
	constructor(private themeService: ThemeService,
					    @Attribute('new-tx') public newTx: boolean | null,) {
		this.newTx = (newTx != null);
	  this.themeSubscription = this.themeService.theme.subscribe(theme => {
	    this.theme = theme;
	  });
	}

  ngOnInit() {
  }

}
