import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges, OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";
import {AuthState} from "../model/auth-state";

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.scss']
})
export class NewTransactionComponent implements AfterViewInit, OnChanges, OnInit, OnDestroy {
  private themeSubscription: Subscription;
  public theme: Theme;
  public authState = AuthState;

  public windowWidth: number;
  public windowHeight: number;

  public showFixedCircle = false;
  public showCloseButton = false;
  public hidingFixedCircle = false;
  public fixedCircleStyle = {};
  public newTxInfoStyle = {};
  public circleTransitionTime = .5;
  public circleTransitionTimeString = this.circleTransitionTime + 's';

  public sendingTx = false;

  @ViewChild('fixed') _fixed: ElementRef;

  @Input('mode') mode;
  @Input('transaction') transaction;
  @Input('userPreferences') userPreferences;
  @Input('newTxStyle') newTxStyle;
  @Input('newTxCircleStyle') newTxCircleStyle;
  @Input('small') small;
  @Input('currentAuth') currentAuth;
  @Input('txListLoaded') txListLoaded;
  @Input('absoluteCircleInfo') absoluteCircleInfo;

  @Output() setMode: EventEmitter<string> = new EventEmitter<string>();
  @Output() sentTx: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;
    if (this.mode === 'approve') {
      this.callibrateFixedCircle();
    }
  }

  constructor(
    private themeService: ThemeService
  ) {
    // this.assetSubscription = this.assetService.ethereumAssets.subscribe(assets => {
    // 	console.log('asset subscription triggered')
    // 	this.callibrateCircles();
    // });
  }

  callibrateFixedCircle() {
    // this repositions the fixed circle prior to fullscreening

    this.fixedCircleStyle['transition'] = this.circleTransitionTimeString;
    if (this.mode === 'initial' && this.absoluteCircleInfo) {
      this.fixedCircleStyle['width'] = this.absoluteCircleInfo.width;
      this.fixedCircleStyle['height'] = this.absoluteCircleInfo.height;
      this.fixedCircleStyle['left'] = this.absoluteCircleInfo.totalOffsetLeft + 'px';
      this.fixedCircleStyle['top'] = this.absoluteCircleInfo.totalOffsetTop + 'px';
    } else if (this.mode === 'approve') {
      const r = this.fullscreenCircleRadius();
      const fixedCircleLeftOffset = (r - this.windowWidth / 2) * -1;
      const fixedCircleTopOffset = (r - this.windowHeight / 2) * -1;


      // setTimeout(() => {
        this.fixedCircleStyle['transition'] = this.circleTransitionTimeString;
        this.fixedCircleStyle['width'] = r * 2 + 'px';
        this.fixedCircleStyle['height'] = r * 2 + 'px';
        this.fixedCircleStyle['left'] = fixedCircleLeftOffset + 'px';
        this.fixedCircleStyle['top'] = fixedCircleTopOffset + 'px';
      // }, 0);
    }
  }

  fullscreenCircleRadius() {
    const r = Math.sqrt(
      Math.pow(
        Math.max(this.windowWidth, this.windowHeight) / 2, 2
      ) +
      Math.pow(
        Math.min(this.windowWidth, this.windowHeight) / 2, 2
      )
    );
    return r;
  }

  clickClose() {
    this.showCloseButton = false;
    this.cancelTx();
  }

  cancelTx() {
    this.setMode.emit('initial');
  }

  submitTx() {
    this.setCircleToTxHistory();
    this.sendingTx = true;
    this.sentTx.emit(true);
  }

  setCircleToTxHistory() {
    const r = this.fullscreenCircleRadius();
    const fixedCircleTopOffset = (r * 1.25 + this.windowHeight) * -1;
    this.fixedCircleStyle['top'] = fixedCircleTopOffset + 'px';
    this.newTxInfoStyle['top'] = fixedCircleTopOffset + 'px';
    // this.fixedCircleStyle['transition'] = this.circleTransitionTime * 4 + 's';
    // this.fixedCircleStyle['width'] = 0 + 'px';
    // this.fixedCircleStyle['height'] = 0 + 'px';
    // this.fixedCircleStyle['transform'] = 'scale(0) translateY(-100vh)';
    this.showCloseButton = false;
    setTimeout(() => {
      this.showFixedCircle = false;
      this.fixedCircleStyle['transform'] = 'none';
      this.newTxInfoStyle['top'] = '0px';
    }, this.circleTransitionTime * 4000);
  }

  setCircleToFullscreen() {
    const r = this.fullscreenCircleRadius();
    const fixedCircleLeftOffset = (r - this.windowWidth / 2) * -1;
    const fixedCircleTopOffset = (r - this.windowHeight / 2) * -1;
    this.fixedCircleStyle['width'] = this.absoluteCircleInfo.width + 'px';
    this.fixedCircleStyle['height'] = this.absoluteCircleInfo.height + 'px';
    this.fixedCircleStyle['left'] = this.absoluteCircleInfo.totalOffsetLeft + 'px';
    this.fixedCircleStyle['top'] = this.absoluteCircleInfo.totalOffsetTop + 'px';
    this.fixedCircleStyle['transition'] = this.circleTransitionTimeString;


    setTimeout(() => {
      this.fixedCircleStyle['width'] = r * 2 + 'px';
      this.fixedCircleStyle['height'] = r * 2 + 'px';
      this.fixedCircleStyle['left'] = fixedCircleLeftOffset + 'px';
      this.fixedCircleStyle['top'] = fixedCircleTopOffset + 'px';
    }, 0);
  }

  cancelCircleFullscreen() {
    this.fixedCircleStyle['transition'] = this.circleTransitionTimeString;
    const contentOffsetLeft = this.absoluteCircleInfo.totalOffsetLeft;
    const contentOffsetTop = this.absoluteCircleInfo.totalOffsetTop;
    this.fixedCircleStyle['width'] = this.absoluteCircleInfo.width + 'px';
    this.fixedCircleStyle['height'] = this.absoluteCircleInfo.height + 'px';
    this.fixedCircleStyle['left'] = this.absoluteCircleInfo.totalOffsetLeft + 'px';
    this.fixedCircleStyle['top'] = this.absoluteCircleInfo.totalOffsetTop + 'px';
    this.hidingFixedCircle = true;
    this.showCloseButton = false;
    setTimeout(() => {
      this.showFixedCircle = false;
      this.hidingFixedCircle = false;
    }, this.circleTransitionTime * 1000);
  }



  setCircle(modeChanges) {
      if (modeChanges.currentValue === 'initial') {
      if (modeChanges.previousValue === 'approve') {
        this.cancelCircleFullscreen();
      } else {
        this.callibrateFixedCircle();
      }
    } else if (modeChanges.currentValue === 'approve') {
      this.showCloseButton = true;
      this.showFixedCircle = true;
      this.setCircleToFullscreen();
    } else if (modeChanges.currentValue === 'send') {
      // this.setCircleToTxHistory();
      this.submitTx();
    }
  }


  ngOnChanges(changes: SimpleChanges) {
    if (changes.mode && this.absoluteCircleInfo) {
      this.setCircle(changes.mode);
    }
    if (changes.txListLoaded && this.sendingTx) {
      console.log('changes.txListLoaded && this.sendingTx');
      if (changes.txListLoaded.currentValue == true) {
        this.sendingTx = false;
        // this.setCircleToTxHistory();
      }
    }

    if (changes.absoluteCircleInfo && changes.absoluteCircleInfo.currentValue) {
      // this.setCircle(changes.mode);
      // this.absoluteCircleInfo = changes.absoluteCircleInfo.currentValue;
    }
  }

  ngAfterViewInit() {
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;
    this.callibrateFixedCircle();
  }

  ngOnInit() {
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
