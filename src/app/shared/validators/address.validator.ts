import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

declare var ethereumjs;

export const AddressValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  let ret: any = null;
  const val = control.value;
  if (!ethereumjs.Util.isValidAddress(val)) {
    ret = {'address': 'Not a valid checksummed address.'};
  }
  return ret;
};

