import { AbstractControl } from '@angular/forms';
import { BigNumber } from "bignumber.js";
import {isNullOrUndefined} from "util";

export function NonNegative(control: AbstractControl) {
  let ret: any = null;

  if (isNullOrUndefined(control.value)) {
    return ret;
  }

  if (typeof control.value === 'string') {
    if (control.value === "" || control.value === ".") {
      return ret;
    }
    const num = new BigNumber(control.value);
    if (num.lt(new BigNumber('0'))) {
      ret = { negative: 'Amount cannot be negative.' };
    }
  } else if (control.value.constructor.name === 'BigNumber') {
    if (control.value.lt(new BigNumber('0'))) {
      ret = { negative: 'Amount cannot be negative.' };
    }
  } else if (typeof control.value === 'number') {
    if (control.value < 0) {
      ret = { negative: 'Amount cannot be negative.' };
    }
  }
  return ret;
}
