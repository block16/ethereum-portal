import {
  AfterContentInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {Theme} from "../model/theme/theme";
import {ThemeService} from "../../core/theme.service";
import {NotificationService} from '../../core/notification.service';

declare var ethereumjs;

@Component({
  selector: 'app-receive',
  templateUrl: './receive.component.html',
  styleUrls: ['./receive.component.scss']
})
export class ReceiveComponent implements AfterContentInit, OnInit, OnDestroy {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setQrSize();
  }

  @Input('address')
  set setAddress(value: string) {
    this.address = ethereumjs.Util.addHexPrefix(value);
  }
  @ViewChild('qrBox') _qrBox: ElementRef;

  public qrSize: string = null;
  public address: string;

  public theme: Theme;
  private themeSubscription: Subscription;

  constructor(private themeService: ThemeService,
              private el: ElementRef,
              private notify: NotificationService,) {
    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
    });
  }

  setQrSize() {
    this.qrSize = Math.min(this._qrBox.nativeElement.offsetWidth, (window.innerHeight * .5)).toString();
  }

  ngAfterContentInit() {
    this.setQrSize();
  }

  ngOnInit() {
    this.setQrSize();
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}
