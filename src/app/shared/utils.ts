import {EthereumTokens, TokenData} from "../core/static-models/ethereum-tokens";
import {BigNumber} from "bignumber.js";

declare var ethereumjs;
declare var Web3;

const gweiToWei = new BigNumber('1000000000');

export function privateKeyToAddress(privateKey: string): string {
  if (privateKey.length !== 64) {
    throw new Error("Private key was not length 64, was length: " + privateKey.length);
  }
  return ethereumjs.Util.privateToAddress(ethereumjs.Util.toBuffer(ethereumjs.Util.addHexPrefix(privateKey))).toString('hex');
}

export function checkTokenSymbol(address): TokenData {
  return EthereumTokens.find(obj => {
    return ethereumjs.Util.addHexPrefix(obj.contractAddress.toLowerCase()) === ethereumjs.Util.addHexPrefix(address.toLowerCase());
  });
}

/**
 * Helper to specifically assist with having full byte representations
 * @desc the toHex function from web3 will remove leading 0s from the hex representation
 * but these transactions will be rejected at the provider level.
 * @param {number | string | BigNumber} n
 * @returns {string}
 */
export function toHexPadded(n: number|string|BigNumber): string {
  let hex = Web3.utils.toHex(n).substr(2);
  if (typeof n === 'string') {
    hex = new BigNumber(n).toString(16);
  } else if (typeof n === 'number') {
    hex = new BigNumber(n.toString()).toString(16);
  } else if (n instanceof BigNumber) {
    hex = n.toString(16);
  }
  // Need full byte two hex numbers with leading zero for sending some transactions
  hex = hex.length % 2 !== 0 ? '0' + hex : hex;
  return '0x' + hex;
}

export function toWeiFromGweiHex(n: string): string {
  return toHexPadded(new BigNumber(n).times(gweiToWei));
}

export function toWeiFromGwei(n: string): string {
  return new BigNumber(n).times(gweiToWei).toString();
}

export function calculateGasPrice(gasLimit: number|string|BigNumber, gasPrice: number|string|BigNumber): BigNumber {
  const gL = new BigNumber(gasLimit.toString());
  const gP = new BigNumber(toWeiFromGwei(gasPrice.toString()));
  const places = new BigNumber(10).pow(18);
  return gL.times(gP).div(places);
}
