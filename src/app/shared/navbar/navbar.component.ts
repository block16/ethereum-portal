import {AfterViewChecked, AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataShareService} from "../../core/data-share.service";
import {Subscription} from "rxjs/Subscription";
import {Theme} from "../model/theme/theme";
import {ThemeService} from "../../core/theme.service";
import {isNullOrUndefined} from "util";
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";
import {NotificationService} from '../../core/notification.service';

declare var ethereumjs;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  host: {
    '[style.background]': `this.theme.secondaryColor`,
    '[style.color]': 'this.theme.primaryColor',
    '[class.show]': 'this.dataLoaded'
  }
})
export class NavbarComponent implements OnInit, OnDestroy {

  @ViewChild('indicator') _indicator: ElementRef;
  @ViewChild('history') _history: ElementRef;
  @ViewChild('send') _send: ElementRef;
  @ViewChild('receive') _receive: ElementRef;

  public navLocation: string;
  public theme: Theme;
  public ethereumAddress: string;
  public navIndicatorLeft: number;
  public navIndicatorWidth: number;
  public dataLoaded = false;

  private ethereumAddressSubscription: Subscription;
  private themeSubscription: Subscription;
  private dataShareSubscription: Subscription;
  private dataLoadedSubscription: Subscription;


  public showQRModal = false;

  public windowWidth: number;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowWidth = window.innerWidth;
    if (this.dataShareService.isDesktopSize(this.windowWidth) && this.dataShareService.getNavLocation() != 'send') {
      this.dataShareService.setNavLocation('send');
    }
    this.moveNavIndicator(this.navLocation);
  }

  constructor(
    private themeService: ThemeService,
    private dataShareService: DataShareService,
    private coreKeyManagerService: CoreKeyManagerService,
    private notify: NotificationService,
  ) {
    this.ethereumAddressSubscription = this.coreKeyManagerService.currentAddress.subscribe(a => this.ethereumAddress = ethereumjs.Util.addHexPrefix(a));

    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
    });

    this.dataLoadedSubscription = this.dataShareService.dataLoaded.subscribe(dataLoaded => {
      this.dataLoaded = dataLoaded;
    });
  }

  ngOnInit(): void {
    this.dataShareSubscription = this.dataShareService.navLocation.subscribe((value: any) => {
      this.navLocation = value;
      if (this._indicator) {
        this.moveNavIndicator(value);
      }
    });
  }

  ngOnDestroy(): void {
    if (!isNullOrUndefined(this.dataShareSubscription)) {
      this.dataShareSubscription.unsubscribe();
    }
  }

  copySuccess() {
    this.notify.message('Address copied to clipboard.', 'Address copied');
  }

  moveNavIndicator(location) {
    // setTimeout(() => {
      if (location === 'history') {
        this.navIndicatorLeft = this._history.nativeElement.getBoundingClientRect().left;
        this.navIndicatorWidth = this._history.nativeElement.getBoundingClientRect().width;
      } else if (location === 'send') {
        this.navIndicatorLeft = this._send.nativeElement.getBoundingClientRect().left;
        this.navIndicatorWidth = this._send.nativeElement.getBoundingClientRect().width;
      } else if (location === 'receive') {
        this.navIndicatorLeft = this._receive.nativeElement.getBoundingClientRect().left;
        this.navIndicatorWidth = this._receive.nativeElement.getBoundingClientRect().width;
      }
    // }, 0);

  }

  setNavLocation(location) {
    this.dataShareService.setNavLocation(location);
  }
}
