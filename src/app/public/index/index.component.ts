import {
  Component, ElementRef, EventEmitter, HostListener, OnDestroy, OnInit, Output,
  ViewChild
} from '@angular/core';
import {DataShareService} from "../../core/data-share.service";
import {LedgerService} from '../../core/key-manager-services/ledger.service';
import {TrezorConnectService} from '../../core/key-manager-services/trezor-connect.service';
import {Web3Service} from "../../core/web3.service";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";
import {Subscription} from "rxjs/Subscription";
import {Block16Service} from "../../core/block16.service";
import {EthereumAsset} from "../../shared/model/ethereum-asset";
import {UserPreferencesService} from "../../core/user-preferences.service";
import {UserPreferences} from "../../shared/model/user-preferences";
import {AuthState} from '../../shared/model/auth-state';
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";
import {EthereumTransaction} from "../../shared/model/ethereum-transaction";
import {MetamaskService} from "../../core/key-manager-services/metamask.service";
import {BigNumber} from "bignumber.js";

declare var ethereumjs;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  host: {
    '[class.intro]': "this.currentAuth == AuthState.none",
  }
})
export class IndexComponent implements OnInit, OnDestroy {
  @ViewChild('_recentTransactions') _recentTransactions: ElementRef;
  @ViewChild('sendForm') _sendForm: ElementRef;
  @Output() showSidebarChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  public AuthState = AuthState;
  public currentAuth: AuthState;
  private currentAuthSubscription: Subscription;

  // Document states
  private windowHeight: number;
  private windowWidth: number;
  private windowMin: number;
  private windowMax: number;

  // UI states
  public navLocation = 'history';
  public showApproveTransaction = false;
  public showTokenTray = false;
  public newTransactionState = 'initial';
  public hoverLedger = false;
  public hoverMetaMask = false;
  public hoverTrezor = false;
  public hoverUTC = false;
  public sendingTx = false;
  public txListLoaded = false;
  private dataLoadedSubscription: Subscription;
  public dataLoaded = false;

  // User preferences
  private userPreferencesSubscription: Subscription;
  public userPreferences: UserPreferences;
  private themeSubscription: Subscription;
  public theme: Theme;

  // Form states
  public sendAddress: string;
  public sendAmount: number;
  public sendAsset = -1;

  // Modal states
  public showWalletInfoModal = false;
  public showFAQModal = false;
  public showDisclaimerModal = false;
  public showTokenTrayModal = false;

  // User info
  private assetSubscription: Subscription;
  public assets: EthereumAsset[];
  public etherAsset: EthereumAsset;
  public tokens: EthereumAsset[];
  public assetCount = 0;
  public recentTransactions = [];

  public newTransaction: EthereumTransaction;

  // Styles
  public newTransactionStyle = {};
  public newTransactionCircleStyle = {};

  public porfolioBalance: BigNumber;
  public ethereumAddress: string;
  private ethereumAddressSubscription: Subscription;
  private portfolioBalanceSubscription: Subscription;

  constructor(
    private themeService: ThemeService,
    private userPreferencesService: UserPreferencesService,
    private dataShareService: DataShareService,
    private web3Service: Web3Service,
    private ledgerService: LedgerService,
    private trezorService: TrezorConnectService,
    private metaMaskService: MetamaskService,
    private block16Service: Block16Service,
    private coreKeyManagerService: CoreKeyManagerService,
  ) {
    this.currentAuth = AuthState.none;
    this.assets = [];

    this.ethereumAddress = '';
    this.porfolioBalance = new BigNumber('0');

    // Theme
    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);

    // Assets
    this.assetSubscription = this.block16Service.ethereumAssets.subscribe(assets => {

      if (this.currentAuth !== AuthState.none) {
        this.dataShareService.dataLoaded.next(true);
      }
      
      this.assets = assets;
      this.etherAsset = assets[0];
      this.assetCount = this.assets.filter(a => (!a.isETH() && a.hasBalance())).length;
      this.tokens = this.pruneTokens(this.assets);
      

      });

    this.dataLoadedSubscription = this.dataShareService.dataLoaded.subscribe(dataLoaded => {
      this.dataLoaded = dataLoaded;
    });

    this.block16Service.transactions.subscribe((value: any) => {
      this.recentTransactions = value;
    });

    // TODO: shouldn't this be managed by URLs instead of a service?
    this.dataShareService.navLocation.subscribe((value: any) => {
      if (value === 'send' && this.navLocation !== 'send') {

      }
      this.navLocation = value;
    });

    this.userPreferencesSubscription = this.userPreferencesService.userPreferences.subscribe(p => this.userPreferences = p);
    this.ethereumAddressSubscription = this.coreKeyManagerService.currentAddress.subscribe(a => this.ethereumAddress = ethereumjs.Util.addHexPrefix(a));
    this.currentAuthSubscription = this.coreKeyManagerService.authState.subscribe(s => this.currentAuth = s);
    this.portfolioBalanceSubscription = this.block16Service.totalPortfolioValue.subscribe(b => this.porfolioBalance = b);
  }

  pruneTokens(assets) {
    const tokens = assets.filter(a => (!a.isETH() && a.hasBalance()));
    const orderedTokens = tokens.sort((a, b) => {
          if (a.symbol == 'ETH') return 0;
          else if (a.name < b.name) return -1;
          else if (a.name > b.name) return 1;
          else return 0;
        });
    return orderedTokens;
  }

  ngOnInit(): void {
    this.calibratePage();
    this.resetNewTransactionView();
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.assetSubscription.unsubscribe();
    this.userPreferencesSubscription.unsubscribe();
    this.ethereumAddressSubscription.unsubscribe();
  }

  transactionListLoaded(value) {
    setTimeout(() => {
      this.txListLoaded = value;
    }, 0);
  }

  sentTxChange(value) {
    this.sendingTx = value;
    if (value && !this.dataShareService.isDesktopSize(window.innerWidth)) {
      this.dataShareService.setNavLocation('history');
    } else if (!value) {
      this.txListLoaded = false;
    }
  }

  /**
   * When the send-form emits a generated transaction
   * @param {EthereumTransaction} transaction that was generated
   */
  onTransactionGenerated(transaction: EthereumTransaction) {
    this.newTransactionState = 'approve';
    this.newTransaction = transaction;
    this.showApproveTransaction = true;
    // Want single approve step, for non-privateKey this will open on their device
    if (this.currentAuth !== AuthState.privateKey && this.currentAuth !== AuthState.utcFile) {
      try {
        this.coreKeyManagerService.approveAndSend(this.newTransaction).subscribe((txHash) => {
          this.newTransaction.hash = txHash;
          this.block16Service.newPendingTransaction(this.newTransaction);
          this.newTransactionState = 'send';
          // TODO: Make sure we reset the transaction here?
        }, (err) => {
          this.resetNewTransactionView();
        });
      } catch (e) {
        console.log(e);
        this.resetNewTransactionView();
      }
    }
  }

  sendTransaction() {
    this.web3Service.sendRawTransaction(this.newTransaction).subscribe(txHash => {
      this.newTransaction.hash = txHash;
      this.block16Service.newPendingTransaction(this.newTransaction);
      this.resetNewTransactionView();
    });
  }

  resetNewTransactionView() {
    // sets new transaction to original state, ready to be triggered again
    this.newTransactionState = 'initial';
    const r = this.fullTransactionViewCircleRadius();
    const leftOffset = (r - this.windowWidth / 2) * -1;
    const topOffset = (r + this.windowHeight / 2 + this.windowHeight);
    const transactionTransform = 'translate(0px,' + this.windowHeight + 'px) scale(.15)';
    const circleTransform = 'translate(' + leftOffset + 'px,' + topOffset + 'px) scale(.15)';

    this.newTransactionCircleStyle['transition'] = '0s';
    this.newTransactionCircleStyle['width'] = (r * 2) + 'px';
    this.newTransactionCircleStyle['height'] = (r * 2) + 'px';

    this.newTransactionStyle['transform'] = transactionTransform;
    this.newTransactionCircleStyle['transform'] = circleTransform;
  }

  sendTx() {
    this.newTransactionState = 'send';
  }

  metaMaskAuthState() {
    this.metaMaskService.getEthereumAddresses().subscribe((addresses: string[]) => {
      this.currentAuth = AuthState.metamask;
      this.coreKeyManagerService.setCurrentAuth(this.currentAuth, addresses[0]);
    });
  }

  trezorAuthState() {
    this.trezorService.getEthereumAddresses().subscribe((addresses: string[]) => {
      this.currentAuth = AuthState.trezor;
      this.coreKeyManagerService.setCurrentAuth(this.currentAuth, addresses[0]);
    });
  }

  ledgerAuthState() {
    this.ledgerService.getEthereumAddresses().subscribe((addresses: string[]) => {
      this.currentAuth = AuthState.ledger;
      this.coreKeyManagerService.setCurrentAuth(this.currentAuth, addresses[0]);
      this.ethereumAddress = addresses[0];
    });
  }

  fullTransactionViewCircleRadius() {
    return Math.sqrt(Math.pow(this.windowMax / 2, 2) + Math.pow(this.windowMin / 2, 2));
  }

  resizeTokenTray() {
    if (this.windowWidth > this.dataShareService.tabletMaxBreakPoint) {
      this.showTokenTray = true;
    }
  }

  calibratePage() {
    this.windowHeight = window.innerHeight;
    this.windowWidth = window.innerWidth;
    this.windowMin = Math.min(this.windowWidth, this.windowHeight);
    this.windowMax = Math.max(this.windowWidth, this.windowHeight);
    this.resizeTokenTray();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowHeight = event.target.innerHeight;
    this.windowWidth = event.target.innerWidth;
    this.calibratePage();
  }
}
