import {
  AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, Self,
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";
import {Block16Service} from "../../core/block16.service";
import {DataShareService} from "../../core/data-share.service";
import {TransactionInformation} from "../../shared/model/transaction-information";
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";

declare var ethereumjs;

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit, AfterViewInit, OnDestroy {
  public transactions = [];
  public theme: Theme;
  private themeSubscription: Subscription;
  private transactionSubscription: Subscription;
  private pendingSubscription: Subscription;

  public pendingTransactions: TransactionInformation[];

  private ethereumAddressSubscription: Subscription;
  public ethereumAddress: string;

  public dotInfo = {};

  @Input('incomingTx') incomingTx = false;
  @Output() dotLocation: EventEmitter<any> = new EventEmitter<any>();
  @Output() loaded: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  public window = {'width': window.innerWidth, 'height': window.innerHeight};

  constructor(
    @Self() private host: ElementRef,
    private themeService: ThemeService,
    private block16Service: Block16Service,
    public data: DataShareService,
    private coreKeyManagerService: CoreKeyManagerService,
  ) {

  }

  ngAfterViewInit() {
    this.loaded.emit(true);
    if (this.incomingTx) {
      this.dotLocation.emit(this.dotInfo);
    }
  }

  ngOnInit() {
    this.ethereumAddressSubscription = this.coreKeyManagerService.currentAddress.subscribe(a => this.ethereumAddress = ethereumjs.Util.addHexPrefix(a));
    this.pendingSubscription = this.block16Service.pendingTransactions.subscribe(pts => this.pendingTransactions = pts);
    this.themeSubscription = this.themeService.theme.subscribe(theme => this.theme = theme);
    this.transactionSubscription = this.block16Service.transactions.subscribe(ts => this.transactions = ts);
  }

  ngOnDestroy(): void {
    this.loaded.emit(false);
    this.themeSubscription.unsubscribe();
    this.transactionSubscription.unsubscribe();
  }
}
