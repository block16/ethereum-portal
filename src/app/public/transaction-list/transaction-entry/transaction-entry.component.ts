import {
  AfterViewInit, Attribute, Component, ElementRef, EventEmitter, HostListener, Input, KeyValueDiffer, OnChanges,
  OnDestroy, OnInit,
  Output,
  SimpleChanges, ViewChild
} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {TransactionInformation} from "../../../shared/model/transaction-information";
import {DataShareService} from "../../../core/data-share.service";
import {TransactionService, ListWidths} from "../transaction.service";

@Component({
  selector: 'app-transaction-entry',
  templateUrl: './transaction-entry.component.html',
  styleUrls: ['./transaction-entry.component.scss'],
  host: {
    '[style.background]': `this.hover ?
      'rgba(' +
      this.theme.primaryColorRgb.r + ',' +
      this.theme.primaryColorRgb.g + ',' +
      this.theme.primaryColorRgb.b + ',' +
      this.theme.op1 * .5 +
      ')' :
      'transparent'`
  }
})
export class TransactionEntryComponent {
  @Input() transaction: TransactionInformation;
  @Input() theme;

  @Output() loaded = new EventEmitter();

  public hover = false;

  public monthNames = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];

  constructor(
    @Attribute('pending') public pending: boolean | null,
    public data: DataShareService
  ) {
    this.pending = (pending != null);
  }

  addZeroX(address: string) {
    if (address && !address.match('^0x')) {
      return '0x' + address;
    } else {
      return address;
    }
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.hover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.hover = false;
  }
}
