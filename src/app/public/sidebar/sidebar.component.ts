import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {Web3Service} from "../../core/web3.service";
import {DataShareService} from "../../core/data-share.service";
import {FormBuilder, FormGroup, FormsModule} from '@angular/forms';
import {ThemeService} from "../../core/theme.service";
import {Theme} from "../../shared/model/theme/theme";
import {UserPreferencesService} from "../../core/user-preferences.service";
import {UserPreferences} from "../../shared/model/user-preferences";
import {Provider} from "../../shared/model/providers";
import {Block16Service} from "../../core/block16.service";
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";
import {AuthState} from "../../shared/model/auth-state";
import {Denominations} from "../../shared/model/denomination";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnDestroy {
  @Input() showSidebar: boolean;

  public AuthState = AuthState;
  public denominations = Denominations.map(d => d.abbreviation);

  public address = "";
  public providers: Provider[];
  public currentProvider: Provider;
  public currentAuth: AuthState;

  private currentAuthSubscription: Subscription;
  private providerSubscription: Subscription;
  private themeSubscription: Subscription;
  private themeChangeSubscription: Subscription;
  private dataLoadedSubscription: Subscription;
  private currentAddressSubscription: Subscription;

  public themes: Theme[];

  // ui-elements
  public dataLoaded = false;
  public autoShowMenuButton = false;
  public userPreferences: UserPreferences;
  public userPrefSubscription: Subscription;
  public recentTransactions = [];
  public theme: Theme;

  // modals
  public showFAQModal = false;
  public showDisclaimerModal = false;
  public showCanaryModal = false;

  // forms
  public themeForm: FormGroup;
  public denominationForm: FormGroup;
  public providerForm: FormGroup;
  private denominationChangeSubscription: Subscription;
  private providerChangeSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private block16Service: Block16Service,
    private web3Service: Web3Service,
    private dataShareService: DataShareService,
    private userPreferencesService: UserPreferencesService,
    private themeService: ThemeService,
    private coreKeyManagerService: CoreKeyManagerService
  ) {
    this.providers = this.web3Service.getProviders();
    this.themes = this.themeService.themes;

    this.dataLoadedSubscription = this.dataShareService.dataLoaded.subscribe(dataLoaded => {
      this.dataLoaded = dataLoaded;
    });

    // Build the theme changing selector
    this.themeForm = this.formBuilder.group({
      'themePreferences': []
    });

    this.denominationForm = this.formBuilder.group({
      'denomination': []
    });

    this.providerForm = this.formBuilder.group({
      'provider': []
    });

    this.themeSubscription = this.themeService.theme.subscribe(t => this.theme = t);

    this.themeChangeSubscription = this.themeForm.get('themePreferences').valueChanges.subscribe((theme: Theme) => {
      this.themeService.setTheme(theme.name);
    });

    this.denominationChangeSubscription = this.denominationForm.get('denomination').valueChanges.subscribe((denomination) => {
      this.userPreferencesService.setAdditionalDenomination(denomination);
    });

    this.providerChangeSubscription = this.providerForm.get('provider').valueChanges.subscribe((provider: Provider) => {
      this.web3Service.setCurrentProvider(provider);
      this.userPreferencesService.setProvider(provider.name);
    });

    this.providerSubscription = this.web3Service.currentProvider.subscribe(p => this.currentProvider = p);
    this.currentAddressSubscription = this.coreKeyManagerService.currentAddress.subscribe(a => this.address = a);
    this.block16Service.pendingTransactions.subscribe(value => this.recentTransactions = value);
    this.userPrefSubscription = this.userPreferencesService.userPreferences.subscribe(v => this.userPreferences = v);
    this.currentAuthSubscription = this.coreKeyManagerService.authState.subscribe(a => {
        this.currentAuth = a;
        if (this.currentAuth !== AuthState.none) {
          setTimeout(() => {
            this.autoShowMenuButton = true;
          }, 500);
        }
      });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.themeChangeSubscription.unsubscribe();
    this.userPrefSubscription.unsubscribe();
    this.providerChangeSubscription.unsubscribe();
    this.providerSubscription.unsubscribe();
    this.currentAuthSubscription.unsubscribe();
    this.currentAddressSubscription.unsubscribe();
  }

  toggleManualGas() {
    this.userPreferencesService.setManualGas(!this.userPreferences.manualGas);
  }

  toggleRandomTheme() {
    this.userPreferencesService.setRandomTheme(!this.userPreferences.randomTheme);
    if (this.userPreferences.randomTheme) {
      const theme = this.themeService.getRandomTheme();
      this.userPreferencesService.setTheme(theme);
      this.themeForm.get('themePreferences').setValue(theme);
    }
  }

  toggleAutoAuth() {
    this.userPreferencesService.setAutoAuth(!this.userPreferences.autoAuth);
  }

  toggleShowPreSigned() {
    this.userPreferencesService.setShowPreSigned(!this.userPreferences.showPreSigned);
  }

  unAuthenticate() {
    this.coreKeyManagerService.resetState();
    this.showSidebar = false;
  }
}
