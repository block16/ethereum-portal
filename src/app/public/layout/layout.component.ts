import {Component, OnDestroy} from '@angular/core';
import {Theme} from "../../shared/model/theme/theme";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Subscription} from "rxjs/Subscription";
import {ThemeService} from "../../core/theme.service";
import {UserPreferencesService} from "../../core/user-preferences.service";
import {UserPreferences} from "../../shared/model/user-preferences";
import {AuthState} from '../../shared/model/auth-state';
import {CoreKeyManagerService} from "../../core/key-manager-services/core-key-manager.service";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  host: {
    '[style.background]': `this.theme.secondaryColor`
  }
})
export class LayoutComponent implements OnDestroy {

  public theme: Theme;
  public showSidebar: boolean;

  public currentAuth: AuthState;
  public AuthState = AuthState;
  public userPreferences: UserPreferences;

  public currentAuthSubscription: Subscription;
  public userPrefSubscription: Subscription;
  private themeSubscription: Subscription;

  constructor(
    private userPreferencesService: UserPreferencesService,
    private themeService: ThemeService,
    private coreKeyManagerService: CoreKeyManagerService,
  ) {
    this.currentAuth = AuthState.none;

    this.showSidebar = false;

    this.userPrefSubscription = this.userPreferencesService.userPreferences.subscribe(value => {
      this.userPreferences = value;
    });

    this.themeSubscription = this.themeService.theme.subscribe(theme => {
      this.theme = theme;
    });

    this.currentAuthSubscription = this.coreKeyManagerService.authState.subscribe(a => this.currentAuth = a);
  }

  public setCurrentAuth(currentAuth) {
    this.currentAuth = currentAuth;
  }

  public ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.userPrefSubscription.unsubscribe();
    this.currentAuthSubscription.unsubscribe();
  }

  public showSidebarChange(b: boolean) {
    this.showSidebar = b;
  }

  public onResize() {

  }
}
