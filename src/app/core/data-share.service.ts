import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class DataShareService {
  public navLocation = new BehaviorSubject('send');
  public dataLoaded = new BehaviorSubject(false);
  public tabletMaxBreakPoint = 1150;
  public mobileMaxBreakPoint = 500;

  constructor() {
  }

  windowSize(windowWidth) {
    if (windowWidth <= this.mobileMaxBreakPoint) {
      return 'mobile';
    } else if (windowWidth > this.mobileMaxBreakPoint && windowWidth <= this.tabletMaxBreakPoint) {
      return 'tablet';
    } else if (windowWidth > this.tabletMaxBreakPoint) {
      return 'desktop';
    }
  }

  isMobileSize(windowWidth) {
    return windowWidth <= this.mobileMaxBreakPoint;
  }

  isTabletSize(windowWidth) {
    return windowWidth > this.mobileMaxBreakPoint && windowWidth <= this.tabletMaxBreakPoint;
  }

  isDesktopSize(windowWidth) {
    return windowWidth > this.tabletMaxBreakPoint;
  }

  getNavLocation(): string {
    return this.navLocation.value;
  }

  setNavLocation(location: string) {
    setTimeout(() => {
      this.navLocation.next(location);
    }, 0);
  }
}
