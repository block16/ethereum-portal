import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {fromPromise} from 'rxjs/observable/fromPromise';
import {EthereumTransaction} from '../shared/model/ethereum-transaction';
import {isNullOrUndefined} from "util";
import {Provider} from "../shared/model/providers";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {BigNumber} from "bignumber.js";
import {UserPreferencesService} from "./user-preferences.service";

declare var Web3;
declare var ethereumjs;

@Injectable()
export class Web3Service {
  private providers = [
    new Provider("Infura", "https://mainnet.infura.io"),
    new Provider("Ropsten", "https://ropsten.infura.io"),
  ];

  public currentProvider: BehaviorSubject<Provider>;
  public web3js: any;

  // encode web3js functions
  public tokenTransferHash;
  public symbolFn;
  public decimalsFn;

  constructor(
    private preferenceService: UserPreferencesService
  ) {
    this.currentProvider = new BehaviorSubject(this.providers[0]);
    this.web3js = new Web3(new Web3.providers.HttpProvider(this.currentProvider.value.location));

    // encode web3js functions
    this.tokenTransferHash = this.web3js.utils.sha3('Transfer(address,address,uint256)');
    this.symbolFn = this.web3js.eth.abi.encodeFunctionSignature('symbol()');
    this.decimalsFn = this.web3js.eth.abi.encodeFunctionSignature('decimals()');

    this.preferenceService.userPreferences.subscribe(prefs => {
      if (!isNullOrUndefined(prefs.provider)) {
        let p = this.providers.find(provider => provider.name === prefs.provider);
        if (isNullOrUndefined(p)) {
          p = this.providers.find(provider => provider.location === prefs.provider);
        }
        this.currentProvider.next(p);
        this.web3js = new Web3(new Web3.providers.HttpProvider(p.location));
      } else {
        this.web3js = new Web3(new Web3.providers.HttpProvider(this.providers[0]));
      }
    });
  }

  erc20Transfer(toAddress: string, value: BigNumber | string): string {
    return this.web3js.eth.abi.encodeFunctionCall({
      "constant": false,
      "inputs": [
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    }, [toAddress, value]);
  }

  public getWebInstance(): any {
    return this.web3js;
  }

  public getBalance(address: string): Observable<BigNumber> {
    return Observable.create((observer) => {
      this.web3js.eth.getBalance(address).then((balance) => {
        observer.next(new BigNumber(balance));
        observer.complete();
      });
    });
  }

  public getProviders(): Provider[] {
    return this.providers;
  }

  public getTokenBalance(contractAddress: string, userAddress: string): Observable<BigNumber> {
    const balanceFn = this.web3js.eth.abi.encodeFunctionCall({
      "constant": true,
      "inputs": [{"name": "_owner", "type": "address"}],
      "name": "balanceOf",
      "outputs": [{"name": "balance", "type": "uint256"}],
      "payable": false,
      "type": "function"
    }, [ethereumjs.Util.addHexPrefix(userAddress)]);

    // TODO: Find out what happens if the contract doesn't have this value...
    return fromPromise(this.web3js.eth.call({
      to: ethereumjs.Util.addHexPrefix(contractAddress),
      data: balanceFn
    })).map((n: string) => new BigNumber(n.substring(2), 16));
  }

  public getTransactionReciept(txHash: string): Observable<any> {
    return fromPromise(this.web3js.eth.getTransactionReceipt(ethereumjs.Util.addHexPrefix(txHash)));
  }

  public getTransactionCount(account: string): Observable<any> {
    return fromPromise(this.web3js.eth.getTransactionCount(ethereumjs.Util.addHexPrefix(account), 'latest'));
  }

  public sendRawTransaction(transaction: EthereumTransaction): Observable<string> {
    if (isNullOrUndefined(transaction.signature)) {
      throw new Error('Transaction signature was missing');
    }
    return Observable.create((observer) => {
      this.web3js.eth.sendSignedTransaction(transaction.signature, (err, txHash) => {
        if (!isNullOrUndefined(err)) {
          observer.error(err);
        } else {
          observer.next(txHash);
        }
        observer.complete();
      });
    });
  }

  public setCurrentProvider(p: Provider) {
    this.preferenceService.setProvider(p.name);
  }

  public getDecimals(a: string): Observable<number> {
    const tokenFn = this.web3js.eth.abi.encodeFunctionSignature('decimals()');
    return fromPromise(this.web3js.eth.call({
      to: ethereumjs.Util.addHexPrefix(a),
      data: tokenFn
    })).map((r: string) => {
      // TODO: Find out what happens if the contract doesn't have this value...
      return new BigNumber(r.substring(2), 16).toNumber();
    });
  }

  public getTokenSymbol(a: string): Observable<string> {
    const tokenFn = this.web3js.eth.abi.encodeFunctionSignature('symbol()');
    return fromPromise(this.web3js.eth.call({
      to: ethereumjs.Util.addHexPrefix(a),
      data: tokenFn
    })).map((r: string) => {
      // TODO: Find out what happens if the contract doesn't have this value...
      return this.web3js.utils.hexToAscii(r).replace(/[^\x20-\x7F]/g, "").trim();
    });
  }

  public getTokenName(a: string): Observable<string> {
    const nameFn = this.web3js.eth.abi.encodeFunctionSignature('name()');
    return fromPromise(this.web3js.eth.call({
      to: ethereumjs.Util.addHexPrefix(a),
      data: nameFn
    })).map((r: string) => {
      // TODO: Find out what happens if the contract doesn't have this value...
      return this.web3js.utils.hexToAscii(r).replace(/[^\x20-\x7F]/g, "").trim();
    });
  }
}
