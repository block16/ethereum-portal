import { Injectable } from '@angular/core';
import {UserPreferences} from "../shared/model/user-preferences";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Theme} from "../shared/model/theme/theme";
import {isNullOrUndefined} from "util";
import {Denominations} from "../shared/model/denomination";

@Injectable()
export class UserPreferencesService {
  static localPreferencesKey = "preferences2";

  public userPreferences: BehaviorSubject<UserPreferences>;

  constructor() {
    let preferencesFromLocal = this.getPreferencesFromLocal();
    if (isNullOrUndefined(preferencesFromLocal)) {
      preferencesFromLocal = <UserPreferences>{
        autoAuth: false,
        manualGas: false,
        showPreSigned: false,
        randomTheme: false,
        denomination: Denominations[1],
        provider: "https://mainnet.infura.io",
        themeName: "Default"
      };
    }
    this.userPreferences = new BehaviorSubject(preferencesFromLocal);
  }

  public setAdditionalDenomination(denomName: string) {
    const preferences = this.userPreferences.value;
    preferences.denomination = Denominations.find(d => d.abbreviation === denomName);
    this.savePreferencesLocal(preferences);
  }

  public setRandomTheme(value: boolean) {
    const preferences = this.userPreferences.value;
    preferences.randomTheme = value;
    this.savePreferencesLocal(preferences);
  }

  public setTheme(theme: Theme) {
    const preferences = this.userPreferences.value;
    preferences.themeName = theme.name;
    this.savePreferencesLocal(preferences);
  }

  public setAutoAuth(autoAuth: boolean) {
    const preferences = this.userPreferences.value;
    preferences.autoAuth = autoAuth;
    this.savePreferencesLocal(preferences);
  }

  public setManualGas(manualGas: boolean) {
    const preferences = this.userPreferences.value;
    preferences.manualGas = manualGas;
    this.savePreferencesLocal(preferences);
  }

  public setProvider(provider: string) {
    const preferences = this.userPreferences.value;
    preferences.provider = provider;
    this.savePreferencesLocal(preferences);
  }

  public setShowPreSigned(showPreSigned: boolean) {
    const preferences = this.userPreferences.value;
    preferences.showPreSigned = showPreSigned;
    this.savePreferencesLocal(preferences);
  }

  private getPreferencesFromBlockchain() {

  }

  private getPreferencesFromLocal() {
    const prefs = localStorage.getItem(UserPreferencesService.localPreferencesKey);
    if (!isNullOrUndefined(prefs)) {
      return JSON.parse(prefs);
    }
    return null;
  }

  private savePreferencesLocal(preferences: UserPreferences) {
    localStorage.setItem(UserPreferencesService.localPreferencesKey, JSON.stringify(preferences));
    this.userPreferences.next(preferences);
  }

  public savePreferencesBlockchain() {

  }

}
