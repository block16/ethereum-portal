import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {EthereumAsset, EthereumAssetBuilder} from "../shared/model/ethereum-asset";
import {HttpClient, HttpParams} from "@angular/common/http";
import {CoreKeyManagerService} from "./key-manager-services/core-key-manager.service";
import {isNullOrUndefined} from "util";
import {EthereumTransaction} from "../shared/model/ethereum-transaction";
import {Web3Service} from "./web3.service";
import {Observable} from "rxjs/Observable";
import {forkJoin} from 'rxjs/observable/forkJoin';
import {TransactionInformation, TransactionInformationBuilder} from "../shared/model/transaction-information";
import {BigNumber} from 'bignumber.js';
import {NotificationService} from "./notification.service";
import {UserPreferencesService} from "./user-preferences.service";
import {UserPreferences} from "../shared/model/user-preferences";
import {Denominations} from "../shared/model/denomination";
import {checkTokenSymbol} from "../shared/utils";
import {WebSocketSubject} from "rxjs/observable/dom/WebSocketSubject";
import {BlockMessage} from "../shared/model/block-message";
import {AssetInfo} from "../shared/model/asset-info";
import {mergeMap, flatMap, map, mergeAll, reduce, merge, concatMap, filter, tap} from "rxjs/operators";
import {of} from "rxjs";

@Injectable()
export class Block16Service {
  public ethereumAssets: BehaviorSubject<EthereumAsset[]>;
  public transactions: BehaviorSubject<TransactionInformation[]>;
  public pendingTransactions: BehaviorSubject<TransactionInformation[]>;
  public totalPortfolioValue: BehaviorSubject<BigNumber>;

  private preferences: UserPreferences;

  private currentAddress: string;

  private webSocket: WebSocketSubject<BlockMessage>;
  private web3js: any;

  constructor(
    private httpClient: HttpClient,
    private coreKeyManager: CoreKeyManagerService,
    private web3Service: Web3Service,
    private notificationService: NotificationService,
    private preferencesService: UserPreferencesService
  ) {
    // Set a web3 instance for convenience here
    this.web3js = this.web3Service.getWebInstance();
    this.currentAddress = "";
    this.pendingTransactions = new BehaviorSubject<TransactionInformation[]>([]);
    this.transactions = new BehaviorSubject<TransactionInformation[]>([]);

    // TODO: Enable websockets instead of polling
    // this.connectWebsocket();

    this.totalPortfolioValue = new BehaviorSubject(new BigNumber('0'));
    this.preferencesService.userPreferences.subscribe(p => this.preferences = p);

    // Subscribe to the address and get the assets for the address on address update
    this.coreKeyManager.currentAddress.subscribe(a => this.addressUpdated(a));
  }

  /*
   * This is a hack because the ethereumAssets array don't exist when sendForm is re-initialized
   */
  public rebuild() {
    this.addressUpdated(this.currentAddress);
  }

  /**
   *
   * @param address
   */
  private addressUpdated(address) {
    this.currentAddress = address;

    // Build basic EthereumAsset
    const assetBuilder = new EthereumAssetBuilder();
    assetBuilder
      .withName("Ethereum")
      .withSymbol('ETH')
      .withAmount(new BigNumber(0))
      .withDecimalPlaces(18)
      .withContractAddress("")
      .withGasLimit(21000);
    const asset = assetBuilder.build();

    // If we don't have an address, don't bother building the assets list
    if (isNullOrUndefined(address) || address === '') {
      this.ethereumAssets = new BehaviorSubject<EthereumAsset[]>([asset]);
    } else {
      this.getAssetsForAddress(address).pipe(
        flatMap(resp => this.getAllRemoteAssetData(resp)),
        flatMap(l => this.getAssetPrices(l)),
        tap(l => this.ethereumAssets.next(l)),
        flatMap(l => this.getTransactionsForAddress(address)),
        flatMap(respArray => forkJoin(respArray.map(r => this.parseRemoteTxData(r)))),
      ).subscribe((txList: TransactionInformation[]) => {
        this.monitorPendingTxs();
        this.monitorNewTxs();
        const failed = this.loadFailedTransactions();
        // combine and sort the lists
        txList = [...failed, ...txList];
        txList.sort((a, b) => b.created.getTime() - a.created.getTime());
        this.transactions.next(txList);
      }, (err) => {
        this.notificationService.error("Failed to get asset information - please notify 'support@block16.io'");
        console.log(err);
      });
    }
  }

  /**
   *  Connects websocket to the current block websocket endpoint
   */

  /* public connectWebsocket() {
    this.webSocket = WebSocketSubject.create('wss://api.block16.io/v1/block/current/ws');
    this.webSocket.subscribe((blockMessage: BlockMessage) => {
      this.processWebsocketBlock(blockMessage);
    }, (err) => {
      this.connectWebsocket();
      console.error(err);
    });
  } */

  /**
   * Get all associated remote data for gathered Ethereum assets
   * @param b16Assets - server response from Block16 asset servers
   * @returns {Observable<EthereumAsset[]>}
   */
  private getAllRemoteAssetData(b16Assets): Observable<EthereumAsset[]> {
    const assetList = [];
    const decimalsObservables = [];
    const symbolObservables = [];
    const nameObservables = [];
    const balanceObservables = [];

    for (let i = 0; i < b16Assets.length; i++) {
      decimalsObservables.push(this.web3Service.getDecimals(b16Assets[i]));
      symbolObservables.push(this.web3Service.getTokenSymbol(b16Assets[i]));
      nameObservables.push(this.web3Service.getTokenName(b16Assets[i]));
      balanceObservables.push(this.web3Service.getTokenBalance(b16Assets[i], this.currentAddress));
    }

    let decimalForked = forkJoin(decimalsObservables);
    let symbolForked = forkJoin(symbolObservables);
    let nameForked = forkJoin(nameObservables);
    let balanceForked = forkJoin(balanceObservables);

    // This is for when the user has no detected assets - non ERC20 Transfer Event
    if (b16Assets.length === 0) {
      decimalForked = of([]);
      symbolForked = of([]);
      nameForked = of([]);
      balanceForked = of([]);
    }

    return Observable.create(obs => {
      forkJoin(
        this.web3Service.getBalance(this.currentAddress),
        decimalForked,
        symbolForked,
        nameForked,
        balanceForked
      ).subscribe(([ethBalance, decimals, symbols, names, balances]) => {
        const ethBuilder = new EthereumAssetBuilder();
        ethBuilder
          .withName("Ethereum")
          .withSymbol('ETH')
          .withAmount(ethBalance)
          .withDecimalPlaces(18)
          .withContractAddress("")
          .withGasLimit(21000);
        const eth = ethBuilder.build();

        // Add eth to the list of assets
        assetList.push(eth);

        for (let i = 0; i < decimals.length; i++) {
          const tokenData = checkTokenSymbol(<string>b16Assets[i]);
          const assetBuilder = new EthereumAssetBuilder()
          assetBuilder
            .withName(<string>names[i])
            .withSymbol(<string>symbols[i])
            .withAmount(<BigNumber>balances[i])
            .withDecimalPlaces(<number>decimals[i])
            .withContractAddress(<string>b16Assets[i])
            .withGasLimit(65000)
            .withHasSymbol(!!tokenData);

          const asset = assetBuilder.build();

          assetList.push(asset);
        }
        obs.next(assetList);
        obs.complete();
      }, (err) => {
        obs.error(err);
        obs.complete();
      });
    });
  }

  /**
   * Takes a new transaction and converts it to a TransactionInformation object
   * @param {EthereumTransaction} ethTx
   * @returns {TransactionInformation}
   */
  private ethTransactionToTransactionInfo(ethTx: EthereumTransaction): TransactionInformation {
    let val = ethTx.asset.calculateAmount(new BigNumber(ethTx.value));
    if (!ethTx.asset.isETH()) {
      val = ethTx.asset.calculateAmount(ethTx.tokenValue);
    }
    return new TransactionInformationBuilder()
      .withToAddress(ethTx.asset.isETH() ? ethTx.toAddress.toLowerCase() : ethTx.tokenToAddress.toLowerCase())
      .withFromAddress(ethTx.fromAddress.toLowerCase())
      .withStatus("processing")
      .withBlockNumber(0)
      .withKind(ethTx.fromAddress.toLowerCase() === this.currentAddress.toLowerCase() ? "to" : "from")
      .withAssetname(ethTx.asset.symbol)
      .withAmount(val)
      .withCreated(new Date().getTime())
      .withHash(ethTx.hash).build();
  }

  /**
   * Check to see if a transaction's status has been updated in the pending transactions list
   * @param {number} i - the index of the transaction to check
   */
  private checkTransactionStatus(i: number) {
    const p = this.pendingTransactions.value;
    this.web3Service.getTransactionReciept(p[i].hash).subscribe((val) => {
      // It's been included the TX hash is defined
      if (!isNullOrUndefined(val)) {
        const message = "Transaction to 0x" + p[i].toAddress + " for " + p[i].amount + " " + p[i].assetName + " was successful.";
        this.notificationService.message(message, "Transaction Success");
        const removedTx = this.updateFromPending(i, "confirmed", val.blockNumber);
        this.newRecentTransaction(removedTx);
        this.getAssetBalances();
      } else if (p[i].greaterThanHour()) {
        console.log("Transaction hasn't been included in block yet: " + p[i].hash);
        // Remove transactions that are polling for more than an hour
        const removedTx = this.updateFromPending(i, "failed", 0);
        // After we moved this TX to the failed, update pending TX localstorage
        this.savePendingTransactions();
        // Add the failed TX to the failed TX list.
        this.updateFailedTransactions(removedTx);
        this.getAssetBalances();
        const message = "Transaction to 0x" + removedTx.toAddress + " for 0x" + removedTx.amount + " " + removedTx.assetName + " has failed.";
        // Notify user of the failure
        this.notificationService.message(message, "Transaction failure");
      }
    });
  }

  /**
   * Initialize the pending transaction listener loop and the behavior subject for transactions
   */
  private monitorPendingTxs() {
    const assetBuilder = new EthereumAssetBuilder();
    assetBuilder
      .withName("Ethereum")
      .withSymbol('ETH')
      .withAmount(new BigNumber(0))
      .withDecimalPlaces(18)
      .withContractAddress("")
      .withGasLimit(21000);
    const asset = assetBuilder.build();

    const ethAsset = [asset];
    this.ethereumAssets = new BehaviorSubject<EthereumAsset[]>(ethAsset);
    this.pendingTransactions.next(this.loadPendingTransactions());

    // Initialize the recentTransaction checker
    setInterval(() => {
      if (this.currentAddress !== "") {
        const p = this.pendingTransactions.value;
        // Iterate through pending list and see if any have gone through since last visit
        for (let i = p.length - 1; i >= 0; i--) {
          // Check each transaction in the pending list to see if it went through
          this.checkTransactionStatus(i);
        }
      }
    }, 5000);
  }

  private monitorNewTxs() {
    setInterval(() => {
      if (this.currentAddress !== "") {
        // Get the greatest value
        const greatestDate = this.transactions.value.reduce((greatest, current) => {
          return current.created.getTime() > greatest.getTime() ? current.created : greatest;
        }, new Date(0));

        this.getTransactionsForAddress(this.currentAddress, greatestDate).pipe(
          flatMap(t => forkJoin(t.map(x => this.parseRemoteTxData(x)))),
          map(
            tis => tis.filter(ti => ti.created.getTime() > greatestDate.getTime())
              .reduce((total, current) => total.concat(current), this.transactions.value)
              .sort((a, b) => b.created.getTime() - a.created.getTime())
          ),
          tap((tis: TransactionInformation[]) => {
            if (tis.length > 0) {
              this.getAssetBalances();
            }
          })
        ).subscribe(totalTxs => {
          this.transactions.next(totalTxs);
          this.notificationService.message("New transactions detected.", "Balance Update");
        });
      }
    }, 5000, 5000);
  }

  /**
   * Used to parse information coming down from the websocket API
   * Not currently in use.
   * @param {BlockMessage} blockMessage
   */

  /* private processWebsocketBlock(blockMessage: BlockMessage) {
    const blockNumber = this.web3js.utils.hexToNumber(blockMessage.number);
    const timeNumber = this.web3js.utils.hexToNumber(blockMessage.timestamp) * 1000;
    // For each transaction coming down
    for (let i = 0; i < blockMessage.transactions.length; i++) {
      const tx = blockMessage.transactions[i];
      const receipt = blockMessage.receipts[i];
      let isEthTx = true;
      // Parse the logs for token transactions
      for (let j = 0; j < receipt.logs.length; j++) {
        const log = receipt.logs[j];
        // Check for a token transaction
        if (log.topics.length === 3 && log.topics[0] === this.web3Service.tokenTransferHash) {
          isEthTx = false;
          // Grab relevant info from the ethereum logs
          const from = log.topics[1].substring(log.topics[1].length - 40, log.topics[1].length).toLowerCase();
          const to = log.topics[2].substring(log.topics[2].length - 40, log.topics[2].length).toLowerCase();
          // Check if our currentAddress is either the 'to' or 'from' address
          if (to === this.currentAddress.toLowerCase() || from === this.currentAddress.toLowerCase()) {
            this.findOrBuildEthereumAsset(tx.to).subscribe(asset => {
              const newTxInfo = new TransactionInformation(
                to,
                from,
                "confirmed",
                blockNumber,
                from === this.currentAddress.toLowerCase() ? "to" : "from",
                asset.symbol,
                asset.calculateAmount(new BigNumber(log.data, 16)),
                timeNumber,
                blockMessage.hash
              );
              // Notify user if transaction add was successful,
              // make sure not to update the value if we already did.
              if (this.newRecentTransaction(newTxInfo)) {
                // TODO: ...
                // this.notificationService.message("WebsocketfoundTX---");
                asset.amount = asset.amount.plus(new BigNumber(log.data, 16));
                this.addOrReplaceAsset(asset);
              }
            });
          }
        }
      }
      // Only add the 'ETH' tx if it didn't contain token transfers,
      // have to make special case here.
      if (isEthTx) {
        const from = tx.from;
        const to = tx.to;
        if (to === this.currentAddress.toLowerCase() || from === this.currentAddress.toLowerCase()) {
          const ethAsset = this.findTheEthereumAsset();
          const value = new BigNumber(tx.value, 16);
          const newTxInfo = new TransactionInformation(
            to,
            from,
            "confirmed",
            blockNumber,
            from === this.currentAddress.toLowerCase() ? "to" : "from",
            ethAsset.symbol,
            ethAsset.calculateAmount(value),
            timeNumber,
            blockMessage.hash
          );
          if (this.newRecentTransaction(newTxInfo)) {
            // TODO: ...
            // this.notificationService.message("WebsocketfoundEthTX---");
            ethAsset.amount = ethAsset.amount.plus(value);
            this.addOrReplaceAsset(ethAsset);
          }
        }
      }
    }
  } */

  /**
   * Sorts the provided asset list by symbol
   * @param {EthereumAsset[]} assets
   * @returns {EthereumAsset[]}
   */
  public sortAssets(assets: EthereumAsset[]): EthereumAsset[] {
    assets.sort((a, b) => {
      // Always want ETH in the 0th position
      if (a.symbol === 'ETH') {
        return 1;
      } else if (b.symbol === 'ETH') {
        return -1;
      }
      if (a.symbol < b.symbol) {
        return -1;
      } else if (a.symbol > b.symbol) {
        return 1;
      } else {
        return 1;
      }
    });
    return assets;
  }

  /**
   * Adds a new asset to the assets list or replaces a currently existing one
   * @param {EthereumAsset} asset
   */
  private addOrReplaceAsset(asset: EthereumAsset) {
    let assets = this.ethereumAssets.value;
    const index = assets.findIndex(a => a.contractAddress === asset.contractAddress);
    if (index !== -1) {
      assets.splice(index, 1);
    }
    assets.unshift(asset);
    assets = this.sortAssets(assets);
    this.ethereumAssets.next(assets);
  }

  /**
   * Finds existing or builds a new {EthereumAsset}
   * @param {string} contractAddress
   * @returns {Observable<EthereumAsset>}
   */
  private findOrBuildEthereumAsset(contractAddress: string): Observable<EthereumAsset> {
    return Observable.create(obs => {
      let asset = this.findInAssetListByContract(contractAddress);
      if (isNullOrUndefined(asset)) {
        this.getRemoteAssetInfo(contractAddress).subscribe((info: AssetInfo) => {
          const assetBuilder = new EthereumAssetBuilder();
          assetBuilder
            .withName(info.name)
            .withSymbol(info.symbol)
            .withAmount(new BigNumber('0'))
            .withDecimalPlaces(info.decimals)
            .withContractAddress(contractAddress)
            .withGasLimit(65000);
          asset = assetBuilder.build();

          obs.next(obs);
          obs.complete();
        }, (err) => {
          obs.error(err);
          obs.complete();
        });
      } else {
        obs.next(asset);
        obs.complete();
      }
    });
  }

  /**
   * Finds an asset by its address in the assetList
   * @param {string} a - the address of the asset to find
   * @param {EthereumAsset[]} assetList - the list of assets to search
   * @returns {EthereumAsset} the found asset
   */
  private findInAssetListByContract(a: string, assetList?: EthereumAsset[]): EthereumAsset {
    const list = isNullOrUndefined(assetList) ? this.ethereumAssets.value : assetList;
    return list.find(asset => {
      if (isNullOrUndefined(asset.contractAddress) || asset.contractAddress === "") {
        return null;
      }
      return asset.contractAddress.toLowerCase() === a.toLowerCase();
    });
  }

  /**
   * Finds the ETH asset within the list of the assets
   * @returns {EthereumAsset | undefined}
   */
  private findTheEthereumAsset() {
    return this.ethereumAssets.value.find(a => a.symbol === 'ETH');
  }

  /**
   * Gets the assets associated with a particular address from the block16 api
   * @param {string} address
   * @returns {Observable<any>}
   */
  private getAssetsForAddress(address: string): Observable<any[]> {
    const assetUrl = 'https://api.block16.io/v1/address/' + address + '/assets';
    return this.httpClient.get<any[]>(assetUrl);
  }

  private getAssetBalances() {
    const ethBalanceObs = this.web3Service.getBalance(this.currentAddress);
    let tempAssets = this.ethereumAssets.value;
    const assetBalanceObs = forkJoin(
      tempAssets.filter(a => !a.isETH()).map(a => this.web3Service.getTokenBalance(a.contractAddress, this.currentAddress))
    );
    const assetAddresses = of(tempAssets.filter(a => !a.isETH()).map(a => a.contractAddress));

    forkJoin(
      ethBalanceObs,
      assetBalanceObs,
      assetAddresses
    ).subscribe(([e, a, addrs]) => {
      const ethAsset = this.findTheEthereumAsset();
      ethAsset.amount = e;
      tempAssets = this.ethereumAssets.value;
      for (let i = 0; i < a.length; i++) {
        const foundAsset = this.findInAssetListByContract(addrs[i], tempAssets);
        foundAsset.amount = a[i];
      }
    });
  }

  /**
   * Gets the current value of all the assets associated with an account
   */
  private getAssetPrices(assets: EthereumAsset[]): Observable<EthereumAsset[]> {
    const currencies = Denominations.map(d => d.abbreviation);

    let priceUrl = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=';
    priceUrl += assets.map(a => a.symbol).join(',');
    priceUrl = priceUrl + '&tsyms=' + currencies.join(',');

    // Get the prices for all the assets in this account
    return this.httpClient.get(priceUrl).map(prices => {
      const tempAssets = assets;
      Object.keys(prices).forEach(k => {
        const index = tempAssets.findIndex(a => a.symbol.toLowerCase() === k.toLowerCase());
        if (index !== -1) {
          const tempObj = {};
          Object.keys(prices[k]).forEach(symbol => tempObj[symbol] = <number>prices[k][symbol]);
          tempAssets[index].prices = tempObj;
        }
      });
      // Make sure we set 0 value price for tokens that aren't listed
      for (let i = 0; i < tempAssets.length; i++) {
        if (isNullOrUndefined(tempAssets[i].prices)) {
          const tempObj = {};
          currencies.forEach(c => tempObj[c] = 0.0);
          tempAssets[i].prices = tempObj;
        }
      }
      return tempAssets;
    });
  }

  private updateTotalValue() {
    let val = new BigNumber('0');
    this.ethereumAssets.value.forEach(v => {
      val = val.plus(v.value(this.preferences.denomination.abbreviation));
    });
    this.totalPortfolioValue.next(val);
  }

  /**
   * Get the gas price from block16 servers
   * @returns {Observable<BigNumber>}
   */
  public getGasPrice(): Observable<BigNumber> {
    const gasUrl = 'https://api.block16.io/v1/gasPrice';
    return this.httpClient.get(gasUrl).map((response: any) => new BigNumber(response.safeLow));
  }

  /**
   * Retrieve's remote info for a (one) specific ETH token contract address
   * @param {string} contractAddress
   * @returns {Observable<AssetInfo>}
   */
  private getRemoteAssetInfo(contractAddress: string): Observable<AssetInfo> {
    return Observable.create(obs => {
      const name = this.web3Service.getTokenName(contractAddress);
      const decimals = this.web3Service.getDecimals(contractAddress);
      const symbol = this.web3Service.getTokenSymbol(contractAddress);
      // Make all the calls at once and get the result
      forkJoin(name, decimals, symbol).subscribe(([n, d, s]) => {
        const assetInfo = <AssetInfo>{name: n, decimals: d, symbol: s};
        obs.next(assetInfo);
        obs.complete();
      }, err => {
        console.log(err);
        obs.error(err.toString());
        obs.complete();
      });
    });
  }

  /**
   * Gets all the TXs from the remote
   * @param {string} address
   * @param {Date} after which to get results
   * @returns {Observable<any>}
   */
  private getTransactionsForAddress(address: string, after?: Date): Observable<any[]> {
    let params = new HttpParams();
    if (!isNullOrUndefined(after)) {
      params = params.append('after', after.toISOString());
    }
    const assetUrl = 'https://api.block16.io/v1/address/' + address + '/transactions';
    return this.httpClient.get<any[]>(assetUrl, {params: params});
  }

  /**
   * Adds a pending transaction to the pending TXs list to be tracked
   * @param {EthereumTransaction} ethereumTransaction
   */
  public newPendingTransaction(ethereumTransaction: EthereumTransaction) {
    const txs = this.pendingTransactions.value;
    txs.unshift(this.ethTransactionToTransactionInfo(ethereumTransaction));
    this.pendingTransactions.next(txs);
    this.savePendingTransactions();
  }

  /**
   * Adds a new recent {TransactionInfo} to the transaction list if it doesn't exist
   * @param {TransactionInformation} transactionInfo
   * @return {boolean} true if add was successful, false if transaction already exists
   */
  public newRecentTransaction(transactionInfo: TransactionInformation): boolean {
    // Add to TX list even if failed
    const completeTxs = this.transactions.value;
    const existingTx = completeTxs.find(tx => tx.hash === transactionInfo.hash);
    if (isNullOrUndefined(existingTx)) {
      this.removeFromPending(transactionInfo.hash);
      completeTxs.unshift(transactionInfo);
      this.transactions.next(completeTxs);
      return true;
    }
    return false;
  }

  /**
   * Parses the information from the block16 server into TransactionInformation
   * @param data - data from the server
   * @returns {TransactionInformation}
   */
  private parseRemoteTxData(data: any): Observable<TransactionInformation> {
    return Observable.create(obs => {
      let asset = this.findTheEthereumAsset();
      let symbol = "ETH";
      let value: BigNumber = asset.calculateAmount(new BigNumber(data.value));
      if (data.transactionType === "token_transaction") {
        this.findOrBuildEthereumAsset(data.ethereumContract).subscribe(a => {
          asset = a;
          value = asset.calculateAmount(new BigNumber(data.value));
          symbol = asset.symbol;
          const ti = new TransactionInformationBuilder()
            .withToAddress(data.toAddress)
            .withFromAddress(data.fromAddress)
            .withStatus("confirmed")
            .withBlockNumber(data.blockNumber)
            .withKind(data.fromAddress.toLowerCase() === data.address.toLowerCase() ? "to" : "from")
            .withAssetname(symbol)
            .withAmount(value)
            .withCreated(data.transactionDate)
            .withHash(data.transactionHash)
            .build();
          obs.next(ti);
          obs.complete();
        });
      } else {
        const ti = new TransactionInformationBuilder()
          .withToAddress(data.toAddress)
          .withFromAddress(data.fromAddress)
          .withStatus("confirmed")
          .withBlockNumber(data.blockNumber)
          .withKind(data.fromAddress.toLowerCase() === data.address.toLowerCase() ? "to" : "from")
          .withAssetname(symbol)
          .withAmount(value)
          .withCreated(data.transactionDate)
          .withHash(data.transactionHash)
          .build();
        obs.next(ti);
        obs.complete();
      }
    });

  }

  /**
   * Deletes from the pending transaction list without further action
   * @param {string} hash - hash of the tx to remove from pending transactions
   */
  private removeFromPending(hash: string) {
    const p = this.pendingTransactions.value;
    const index = p.findIndex(tx => tx.hash === hash);
    if (index !== -1) {
      p.splice(index, 1);
      this.pendingTransactions.next(p);
      this.savePendingTransactions();
    }
  }

  /**
   * Helper to remove from pending if the transaction
   * @param {number} i - index of the pending transaction
   * @param {string} status - status of the transaction
   * @param {number} blockNumber
   * @returns {TransactionInformation}
   */
  private updateFromPending(i: number, status: string, blockNumber?: number): TransactionInformation {
    const p = this.pendingTransactions.value;
    const tempTxInfo = p[i];
    p.splice(i, 1);
    this.pendingTransactions.next(p);
    this.savePendingTransactions();
    tempTxInfo.status = status;
    tempTxInfo.blockNumber = isNullOrUndefined(blockNumber) ? 0 : blockNumber;
    tempTxInfo.created = new Date();
    return tempTxInfo;
  }

  /**
   * Loads failed transactions from localstorage
   * @returns {TransactionInformation[]}
   */
  private loadFailedTransactions(): TransactionInformation[] {
    let failed: any = localStorage.getItem("failed");
    if (isNullOrUndefined(failed)) {
      failed = [];
    } else {
      failed = JSON.parse(failed);
    }
    let txs = [];
    failed.forEach((tx: any) => {
      txs.push(TransactionInformation.fromJsonRep(tx));
    });
    txs = this.removeNonRelevantTx(txs);
    return txs;
  }

  /**
   * Saves failed transactions into localstorage
   * @param {TransactionInformation} txInfo
   */
  private updateFailedTransactions(txInfo: TransactionInformation): void {
    let failed = this.loadFailedTransactions();
    failed = this.removeNonRelevantTx(failed);
    failed.push(txInfo);
    const jsonRep = [];
    failed.forEach((info: TransactionInformation) => {
      jsonRep.push(info.toJsonRep());
    });
    localStorage.setItem("failed", JSON.stringify(jsonRep));
  }

  /**
   * Loads pending transactions from localstorage
   * @returns {TransactionInformation[]}
   */
  private loadPendingTransactions(): TransactionInformation[] {
    let pending: any = localStorage.getItem("pending");
    if (isNullOrUndefined(pending)) {
      pending = [];
    } else {
      pending = JSON.parse(pending);
    }
    let txs = [];
    pending.forEach((tx: any) => {
      txs.push(TransactionInformation.fromJsonRep(tx));
    });
    txs = this.removeNonRelevantTx(txs);
    return txs;
  }

  /**
   * Saves the pending transactions to the browser localstorage
   */
  private savePendingTransactions(): void {
    const recent = [];
    const p = this.removeNonRelevantTx(this.pendingTransactions.value);
    p.forEach((txInfo: TransactionInformation) => {
      recent.push(txInfo.toJsonRep());
    });
    localStorage.setItem("pending", JSON.stringify(recent));
  }

  /**
   * Removes transactions that don't relate to the current account from the pending / failed stored transactions
   * @param {TransactionInformation[]} tis
   * @returns {TransactionInformation[]}
   */
  private removeNonRelevantTx(tis: TransactionInformation[]): TransactionInformation[] {
    let mutated = tis;
    if (this.currentAddress !== "") {
      mutated = tis.filter(
        ti => ti.fromAddress.toLowerCase() === this.currentAddress.toLowerCase() ||
          ti.toAddress.toLowerCase() === this.currentAddress.toLowerCase()
      );
    }
    return mutated;
  }
}
