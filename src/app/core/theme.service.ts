import {Injectable} from '@angular/core';
import {isNullOrUndefined} from "util";
import {ThemeSource} from "../shared/model/theme-source";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Theme} from "../shared/model/theme/theme";
import {UserPreferencesService} from "./user-preferences.service";
import {UserPreferences} from "../shared/model/user-preferences";

@Injectable()
export class ThemeService {
  private backgroundColor = '#EFF1F2';
  private black = '#000000';
  private yellow = '#f6eb0f';
  private green = '#00CF34';
  private red = '#ED466E';

  public themeSources: ThemeSource[] = [
  {
    'name': 'Amber',
    'primaryColor': '#FF540D',
    'secondaryColor': '#000000',
    'accentColor': '#FF540D',
    'processingColor': '#FF450F',
    'sendColor': '#A22B09',
    'receiveColor': '#FF450F'
  },
  {
    'name': 'Avocado',
    'primaryColor': '#262319',
    'secondaryColor': '#C3D939',
    'accentColor': '#262319',
    'processingColor': '#262319',
    'sendColor': '#996B39',
    'receiveColor': '#6B7524'
  },
  {
    'name': 'B & W',
    'primaryColor': '#000000',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#000000',
    'processingColor': '#000000',
    'sendColor': '#A6A6A6',
    'receiveColor': '#000000'
  },
  {
    'name': 'B & W #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#000000',
    'accentColor': '#FFFFFF',
    'processingColor': '#F3F3F3',
    'sendColor': '#888888',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Banana',
    'primaryColor': '#292716',
    'secondaryColor': '#E6CF00',
    'accentColor': '#292716',
    'processingColor': '#292716',
    'sendColor': '#938a28',
    'receiveColor': '#292716'
  },
  {
    'name': 'Block 16',
    'primaryColor': '#000000',
    'secondaryColor': '#EFF1F2',
    'accentColor': '#000000',
    'processingColor': '#f6eb0f',
    'sendColor': '#E05677',
    'receiveColor': '#1CBC45'
  },
  {
    'name': 'Blueprint',
    'primaryColor': '#0048FF',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#0048FF',
    'processingColor': '#0048FF',
    'sendColor': '#8BABFF',
    'receiveColor': '#0048FF'
  },
  {
    'name': 'Blueprint #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#0048FF',
    'accentColor': '#FFFFFF',
    'processingColor': '#F3F7FF',
    'sendColor': '#83A6FF',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Blush',
    'primaryColor': '#660C39',
    'secondaryColor': '#FFB89E',
    'accentColor': '#660C39',
    'processingColor': '#660C39',
    'sendColor': '#b76b72',
    'receiveColor': '#660C39'
  },
  {
    'name': 'Braun',
    'primaryColor': '#424242',
    'secondaryColor': '#F5F5F5',
    'accentColor': '#ED665A',
    'processingColor': '#E0E0E0',
    'sendColor': '#ED665A',
    'receiveColor': '#424242'
  },
  {
    'name': 'Caramel',
    'primaryColor': '#271C10',
    'secondaryColor': '#FFA024',
    'accentColor': '#271C10',
    'processingColor': '#271C10',
    'sendColor': '#9c672b',
    'receiveColor': '#271C10'
  },
  {
    'name': 'Clay',
    'primaryColor': '#47463E',
    'secondaryColor': '#F2F1EF',
    'accentColor': '#BA350B',
    'processingColor': '#E8D47B',
    'sendColor': '#B83C25',
    'receiveColor': '#22b5cb'
  },
  {
    'name': 'Cloud',
    'primaryColor': '#4B5656',
    'secondaryColor': '#EDF8F6',
    'accentColor': '#6FB2B3',
    'processingColor': '#EDE5C9',
    'sendColor': '#CF9A93',
    'receiveColor': '#8DCCA5'
  },
  {
    'name': 'Cotton Candy',
    'primaryColor': '#475252',
    'secondaryColor': '#F0F3FA',
    'accentColor': '#D685BF',
    'processingColor': '#F5DFE9',
    'sendColor': '#C98AD0',
    'receiveColor': '#6CC496'
  },
  {
    'name': 'Default',
    'primaryColor': '#000000',
    'secondaryColor': '#e0e0cc',
    'accentColor': '#000000',
    'processingColor': '#e86e1c',
    'sendColor': '#872701',
    'receiveColor': '#539B39'
  },
  {
    'name': 'Dark',
    'primaryColor': '#E3E2DE',
    'secondaryColor': this.black,
    'accentColor': '#E3E2DE',
    'processingColor': this.yellow,
    'sendColor': '#DC6466',
    'receiveColor': '#9EE283'
  },
  {
    'name': 'Darker',
    'primaryColor': '#9E9E9E',
    'secondaryColor': '#000000',
    'accentColor': '#9E9E9E',
    'processingColor': '#8D8D8D',
    'sendColor': '#7F7F7F',
    'receiveColor': '#C3C3C3'
  },
  {
    'name': 'Darkroom',
    'primaryColor': '#FF0000',
    'secondaryColor': '#000000',
    'accentColor': '#FF0000',
    'processingColor': '#FF0000',
    'sendColor': '#B00002',
    'receiveColor': '#FF0000'
  },
  {
    'name': 'Earth',
    'primaryColor': '#CCB487',
    'secondaryColor': '#211B1B',
    'accentColor': '#946040',
    'processingColor': '#CCB487',
    'sendColor': '#BD7A51',
    'receiveColor': '#749AB5'
  },
  {
    'name': 'Futura',
    'primaryColor': '#325EC2',
    'secondaryColor': '#FFFCF6',
    'accentColor': '#ED3221',
    'processingColor': '#FFF048',
    'sendColor': '#ED3221',
    'receiveColor': '#325EC2'
  },
  {
    'name': 'Green',
    'primaryColor': '#1DBD00',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#1DBD00',
    'processingColor': '#1DBD00',
    'sendColor': '#82E673',
    'receiveColor': '#1DBD00'
  },
  {
    'name': 'Green #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#1DBD00',
    'accentColor': '#FFFFFF',
    'processingColor': '#FFFFFF',
    'sendColor': '#A7DB9E',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Hologram',
    'primaryColor': '#FF00BF',
    'secondaryColor': '#2E0E61',
    'accentColor': '#FF00BF',
    'processingColor': '#ef26b7',
    'sendColor': '#FD39D3',
    'receiveColor': '#38B7AC'
  },
  {
    'name': 'Hologram #2',
    'primaryColor': '#00FFA6',
    'secondaryColor': '#2E0E61',
    'accentColor': '#00FFA6',
    'processingColor': '#2defa4',
    'sendColor': '#DE53CF',
    'receiveColor': '#37F8C1'
  },
  {
    'name': 'Lavender',
    'primaryColor': '#372137',
    'secondaryColor': '#F7DDF4',
    'accentColor': '#372137',
    'processingColor': '#372137',
    'sendColor': '#B15D93',
    'receiveColor': '#9dad90'
  },
  {
    'name': 'Lavender #2',
    'primaryColor': '#F7DDF4',
    'secondaryColor': '#372137',
    'accentColor': '#F7DDF4',
    'processingColor': '#EFD6EC',
    'sendColor': '#b56fab',
    'receiveColor': '#919e94'
  },
  {
    'name': 'Leet',
    'primaryColor': '#F5007E',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#F5007E',
    'processingColor': '#FFDD18',
    'sendColor': '#F5007E',
    'receiveColor': '#1BCF77'
  },
  {
    'name': 'Leet #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#F5007E',
    'accentColor': '#FFFFFF',
    'processingColor': '#FFFFFF',
    'sendColor': '#F59ECB',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Mint',
    'primaryColor': '#282D30',
    'secondaryColor': '#E6FFF4',
    'accentColor': '#282D30',
    'processingColor': '#B4EDD1',
    'sendColor': '#AC5690',
    'receiveColor': '#50A580'
  },
  {
    'name': 'Mint #2',
    'primaryColor': '#E6FFF4',
    'secondaryColor': '#282D30',
    'accentColor': '#E6FFF4',
    'processingColor': '#B4EDD1',
    'sendColor': '#CC8CC8',
    'receiveColor': '#6DC6AC'
  },
  {
    'name': 'Nu',
    'primaryColor': '#1F1E1B',
    'secondaryColor': '#E8E9DD',
    'accentColor': '#6597EB',
    'processingColor': '#DFDF6F',
    'sendColor': '#C9578A',
    'receiveColor': '#41C170'
  },
  {
    'name': 'Nu #2',
    'primaryColor': '#E8E9DD',
    'secondaryColor': '#1F1E1B',
    'accentColor': '#5568AD',
    'processingColor': '#E4D76E',
    'sendColor': '#944953',
    'receiveColor': '#2F946C'
  },
  {
    'name': 'Orange',
    'primaryColor': '#FF5500',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#FF5500',
    'processingColor': '#FF5500',
    'sendColor': '#FF5500',
    'receiveColor': '#58d68a'
  },
  {
    'name': 'Orange #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#FF5500',
    'accentColor': '#FFFFFF',
    'processingColor': '#FFF6F1',
    'sendColor': '#FCA985',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Peach',
    'primaryColor': '#DA6441',
    'secondaryColor': '#FFEFD9',
    'accentColor': '#FF9C9E',
    'processingColor': '#FFE8A3',
    'sendColor': '#EB6C46',
    'receiveColor': '#91B95C'
  },
  {
    'name': 'Plum',
    'primaryColor': '#FF6BB6',
    'secondaryColor': '#2D043C',
    'accentColor': '#FF6BB6',
    'processingColor': '#FF6BB6',
    'sendColor': '#903976',
    'receiveColor': '#9587CF'
  },
  {
    'name': 'Red',
    'primaryColor': '#FF0000',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#FF0000',
    'processingColor': '#FF0000',
    'sendColor': '#FF0000',
    'receiveColor': '#FFA3A5'
  },
  {
    'name': 'Red #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#FF0000',
    'accentColor': '#FFFFFF',
    'processingColor': '#FFF0F0',
    'sendColor': '#FF9698',
    'receiveColor': '#FFFFFF'
  },
  {
    'name': 'Rhubarb',
    'primaryColor': '#4D665E',
    'secondaryColor': '#F4FFF9',
    'accentColor': '#9E245B',
    'processingColor': '#9E245B',
    'sendColor': '#ACC6B9',
    'receiveColor': '#4D665E'
  },
  {
    'name': 'Salmon',
    'primaryColor': '#54393F',
    'secondaryColor': '#FFC7B5',
    'accentColor': '#54393F',
    'processingColor': '#FFDCC7',
    'sendColor': '#ED665A',
    'receiveColor': '#54393F'
  },
  {
    'name': 'Stone',
    'primaryColor': '#202224',
    'secondaryColor': '#E6EEF2',
    'accentColor': '#202224',
    'processingColor': '#E8E1DE',
    'sendColor': '#A68C77',
    'receiveColor': '#728F6A'
  },
  {
    'name': 'Stone #2',
    'primaryColor': '#E6EEF2',
    'secondaryColor': '#202224',
    'accentColor': '#E6EEF2',
    'processingColor': '#2e2e2f',
    'sendColor': '#9E7F65',
    'receiveColor': '#658C82'
  },
  {
    'name': 'Suede',
    'primaryColor': '#e8d9c5',
    'secondaryColor': '#40171B',
    'accentColor': '#e8d9c5',
    'processingColor': '#4B1E23',
    'sendColor': '#B97A7C',
    'receiveColor': '#e8d9c5'
  },
  {
    'name': 'Suede #2',
    'primaryColor': '#e8e1cc',
    'secondaryColor': '#1C253C',
    'accentColor': '#e8e1cc',
    'processingColor': '#242D46',
    'sendColor': '#878A92',
    'receiveColor': '#e8e1cc'
  },
  {
    'name': 'Summer',
    'primaryColor': '#7a3800',
    'secondaryColor': '#ffd341',
    'accentColor': '#258780',
    'processingColor': '#14CC8E',
    'sendColor': '#636363',
    'receiveColor': '#322c28'
  },
  {
    'name': 'Terminal',
    'primaryColor': '#00BA00',
    'secondaryColor': '#000000',
    'accentColor': '#00BA00',
    'processingColor': '#00BA00',
    'sendColor': '#007000',
    'receiveColor': '#00BA00'
  },
  {
    'name': 'U.F.O.',
    'primaryColor': '#CADBD6',
    'secondaryColor': '#131316',
    'accentColor': '#2AFF99',
    'processingColor': '#2AFF99',
    'sendColor': '#86918C',
    'receiveColor': '#cadbd3'
  },
  {
    'name': 'U.F.O. #2',
    'primaryColor': '#CADBD6',
    'secondaryColor': '#131316',
    'accentColor': '#FF50E4',
    'processingColor': '#FF50E4',
    'sendColor': '#86918C',
    'receiveColor': '#cadbd3'
  },
  {
    'name': 'Vitalik',
    'primaryColor': '#C9DDE1',
    'secondaryColor': '#161B20',
    'accentColor': '#C9DDE1',
    'processingColor': '#E4E9C1',
    'sendColor': '#7d858d',
    'receiveColor': '#C8D6E1'
  },
  {
    'name': 'Zap! Electric shock!',
    'primaryColor': '#0095FF',
    'secondaryColor': '#FFFFFF',
    'accentColor': '#FF13AC',
    'processingColor': '#FFFF2B',
    'sendColor': '#FF66C9',
    'receiveColor': '#0095FF'
  },
  {
    'name': 'Zap! Electric shock! #2',
    'primaryColor': '#FFFFFF',
    'secondaryColor': '#0095FF',
    'accentColor': '#FF13AC',
    'processingColor': '#FFFF2B',
    'sendColor': '#90D1FF',
    'receiveColor': '#FFFFFF'
  },
  ];

  public themes: Theme[];
  public theme: BehaviorSubject<Theme>;
  private preferences: UserPreferences;

  constructor(
    private preferencesService: UserPreferencesService
  ) {
    this.themes = [];
    this.themeSources.forEach((themeSource: ThemeSource) => {
      this.themes.push(ThemeService.constructTheme(themeSource));
    });

    this.theme = new BehaviorSubject(this.themes[0]);

    // Set the default theme initially
    this.preferencesService.userPreferences.subscribe((preferences: UserPreferences) => {
      this.preferences = preferences;
      // if (preferences.randomTheme) {
      //   const theme = this.getRandomTheme();
      // } else {
      //   const theme = this.findThemeByName(preferences.themeName);
      // }
      const theme = this.findThemeByName(preferences.themeName);
      this.theme.next(theme);
    });
  }

  static luma(hex) { // returns a value which represents brightness adjusted for human perception.
    const rgb = this.hexToRgb(hex);
    return (0.2126 * rgb.r) + (0.7152 * rgb.g) + (0.0722 * rgb.b); // SMPTE C, Rec. 709 weightings
  }

  static getContrast(colorA, colorB) {
      return Math.abs(this.luma(colorA) - this.luma(colorB))
  }

  static greaterContrast(hexBase, hex1, hex2) {
    const primaryContrast = this.getContrast(hexBase, hex1);
    const secondaryContrast = this.getContrast(hexBase, hex2);
    const contrastingColor = primaryContrast > secondaryContrast ? hex1 : hex2;
    return contrastingColor;
  }

  static hexToHSL(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    let r = parseInt(result[1], 16);
    let g = parseInt(result[2], 16);
    let b = parseInt(result[3], 16);

    r /= 255, g /= 255, b /= 255;
    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s, l = (max + min) / 2;

    if (max === min) {
      h = s = 0; // achromatic
    } else {
      const d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
    }

    s = s * 100;
    s = Math.round(s);
    l = l * 100;
    l = Math.round(l);
    h = Math.round(360 * h);

    const hsl = {
      'h': h,
      's': s,
      'l': l
    };

    const colorInHSL = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';

    return hsl;
  }

  static hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  static hexToRgba(hex, opacity) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    const rgba = 'rgba(' +
            parseInt(result[1], 16) +
            ',' + parseInt(result[2], 16) +
            ',' + parseInt(result[3], 16) +
            ',' + opacity + ')';
    return rgba;
  }

  static selfAverage(base, multiplier) {
    return base * multiplier / 2;
  }

  static getOpacities(primary, secondary, newTx, processing) {
      const isDark = (this.luma(primary) > this.luma(secondary));
      const newTxDark = (this.luma(newTx) > this.luma(processing));

      const init = .085;
      const endInit = .2;

      const contrast = (this.getContrast(primary, secondary) / 255);

      const opBounds = [
        isDark ?
        init * 2.5 + (init - init * (this.getContrast(primary, secondary) / 255)) :
        init * 1.75 + (init - init * (this.getContrast(primary, secondary) / 255))
        ,
        isDark ?
        1 - (endInit + (endInit - endInit * (this.getContrast(primary, secondary) / 255))) :
        1 - (endInit * 1.75 + (endInit - endInit * (this.getContrast(primary, secondary) / 255)))
      ];

      const newTxOpBounds = [
        newTxDark ?
        init * 2.5 + (init - init * (this.getContrast(primary, secondary) / 255)) :
        init * 1.75 + (init - init * (this.getContrast(primary, secondary) / 255))
        ,
        newTxDark ?
        1 - (endInit + (endInit - endInit * (this.getContrast(newTx, processing) / 255))) :
        1 - (endInit * 1.75 + (endInit - endInit * (this.getContrast(newTx, processing) / 255)))
      ];

      const op1 = opBounds[0];
      const op2 = opBounds[0] + (.333 * (opBounds[1] - opBounds[0]));
      const op3 = opBounds[0] + (.666 * (opBounds[1] - opBounds[0]));
      const op4 = opBounds[1];

      const newTxOp1 = newTxOpBounds[0];
      const newTxOp2 = newTxOpBounds[0] + (.333 * (newTxOpBounds[1] - newTxOpBounds[0]));
      const newTxOp3 = newTxOpBounds[0] + (.666 * (newTxOpBounds[1] - newTxOpBounds[0]));
      const newTxOp4 = newTxOpBounds[1];

      const opacities = {
          'normal': [op1,op2,op3,op4],
          'newTx': [newTxOp1,newTxOp2,newTxOp3,newTxOp4]
      };

      return opacities;
  }

  static constructTheme(themeSource: ThemeSource): Theme {

    const theme: Theme = <Theme>{};

    const primaryColorRgb = ThemeService.hexToRgb(themeSource.primaryColor);
    const secondaryColorRgb = ThemeService.hexToRgb(themeSource.secondaryColor);
    const accentColorRgb = this.hexToRgb(themeSource.accentColor);
    const bgColorRGB = ThemeService.hexToRgb(themeSource.secondaryColor);

    theme.name = themeSource.name;

    theme.primaryColor = themeSource.primaryColor;
    theme.primaryColorRgb = this.hexToRgb(themeSource.primaryColor);
    theme.secondaryColor = themeSource.secondaryColor;
    theme.secondaryColorRgb = this.hexToRgb(themeSource.secondaryColor);
    theme.accentColor = themeSource.accentColor;
    theme.accentColorRgb = this.hexToRgb(themeSource.accentColor);
    theme.newTxColor = this.greaterContrast(themeSource.processingColor, themeSource.primaryColor, themeSource.secondaryColor);
    theme.newTxColorRgb = this.hexToRgb(theme.newTxColor);

    const opacities = this.getOpacities(themeSource.primaryColor, themeSource.secondaryColor, theme.newTxColor, themeSource.processingColor);

    theme.op1 = opacities.normal[0];
    theme.op2 = opacities.normal[1];
    theme.op3 = opacities.normal[2];
    theme.op4 = opacities.normal[3];

    theme.op1Style = {'opacity': opacities.normal[0]};
    theme.op2Style = {'opacity': opacities.normal[1]};
    theme.op3Style = {'opacity': opacities.normal[2]};
    theme.op4Style = {'opacity': opacities.normal[3]};

    theme.newTxOp1 = opacities.newTx[0];
    theme.newTxOp2 = opacities.newTx[1];
    theme.newTxOp3 = opacities.newTx[2];
    theme.newTxOp4 = opacities.newTx[3];

    theme.newTxOp1Style = {'opacity': opacities.newTx[0]};
    theme.newTxOp2Style = {'opacity': opacities.newTx[1]};
    theme.newTxOp3Style = {'opacity': opacities.newTx[2]};
    theme.newTxOp4Style = {'opacity': opacities.newTx[3]};

    theme.primaryColorOp1 = this.hexToRgba(themeSource.primaryColor, theme.op1);
    theme.primaryColorOp2 = this.hexToRgba(themeSource.primaryColor, theme.op2);
    theme.primaryColorOp3 = this.hexToRgba(themeSource.primaryColor, theme.op3);
    theme.primaryColorOp4 = this.hexToRgba(themeSource.primaryColor, theme.op4);

    theme.secondaryColorOp1 = this.hexToRgba(themeSource.secondaryColor, theme.op1);
    theme.secondaryColorOp2 = this.hexToRgba(themeSource.secondaryColor, theme.op2);
    theme.secondaryColorOp3 = this.hexToRgba(themeSource.secondaryColor, theme.op3);
    theme.secondaryColorOp4 = this.hexToRgba(themeSource.secondaryColor, theme.op4);

    theme.accentColorOp1 = this.hexToRgba(themeSource.accentColor, theme.op1);
    theme.accentColorOp2 = this.hexToRgba(themeSource.accentColor, theme.op2);
    theme.accentColorOp3 = this.hexToRgba(themeSource.accentColor, theme.op3);
    theme.accentColorOp4 = this.hexToRgba(themeSource.accentColor, theme.op4);

    theme.newTxColorOp1 = this.hexToRgba(theme.newTxColor, theme.newTxOp1);
    theme.newTxColorOp2 = this.hexToRgba(theme.newTxColor, theme.newTxOp2);
    theme.newTxColorOp3 = this.hexToRgba(theme.newTxColor, theme.newTxOp3);
    theme.newTxColorOp4 = this.hexToRgba(theme.newTxColor, theme.newTxOp4);


    theme.isDark = this.luma(theme.primaryColor) > this.luma(theme.secondaryColor);

    theme.mainStyle = {
      'background': themeSource.secondaryColor,
      'color': themeSource.primaryColor,
      'border-color': themeSource.primaryColor
    };
    theme.inverseMainStyle = {
      'background': themeSource.primaryColor,
      'color': themeSource.secondaryColor,
      'border-color': themeSource.primaryColor
    };
    theme.accentMainStyle = {
      'background': themeSource.accentColor,
      'color': themeSource.secondaryColor,
      'border-color': themeSource.accentColor
    };


    theme.primaryBackgroundStyle = {
      'background-color': themeSource.primaryColor
    };
    theme.secondaryBackgroundStyle = {
      'background-color': themeSource.secondaryColor
    };
    theme.accentBackgroundStyle = {
      'background-color': themeSource.accentColor
    };
    theme.newTxBackgroundStyle = {
      'background-color': theme.newTxColor
    };

    theme.overlayStyle = this.luma(theme.accentColor) > this.luma(theme.secondaryColor) && theme.accentColor == theme.primaryColor ?
    {
      'background': 'rgba('+
                    (secondaryColorRgb.r - 30)+','+
                    (secondaryColorRgb.g - 30)+','+
                    (secondaryColorRgb.b - 30)+','+
                    theme.op3+')',
    } :
    {
      'background': 'rgba('+
                    accentColorRgb.r+','+
                    accentColorRgb.g+','+
                    accentColorRgb.b+','+
                    theme.op3+')',
    };

    theme.primaryTextStyle = {
      'color': themeSource.primaryColor,
      'border-color': themeSource.primaryColor
    };
    theme.secondaryTextStyle = {
      'color': themeSource.secondaryColor,
      'border-color': themeSource.secondaryColor
    };
    theme.accentTextStyle = {
      'color': themeSource.accentColor,
      'border-color': themeSource.accentColor
    };
    theme.newTxTextStyle = {
      'color': theme.newTxColor,
      'border-color': theme.newTxColor
    };

    theme.sectionHeaderStyle = {
      'background': theme.secondaryColor,
      'color': 'rgba(' +
               theme.primaryColorRgb.r + ',' +
               theme.primaryColorRgb.g + ',' +
               theme.primaryColorRgb.b + ',' +
               theme.op4 + ')',
      'border-bottom': '1px solid ' + 'rgba(' +
                       theme.primaryColorRgb.r + ',' +
                       theme.primaryColorRgb.g + ',' +
                       theme.primaryColorRgb.b + ',' +
                       theme.op2 + ')'
    }

    const thinBorder = '2px';
    const fatBorder = '5px';

    theme.textInputFocusStyle = {
      // 'background': theme.primaryColorOp1,
      'border-top': thinBorder + ' solid ' + theme.accentColor,
      'border-bottom': thinBorder + ' solid ' + theme.accentColor,
      'border-left': fatBorder + ' solid ' + theme.accentColor,
      'border-right': thinBorder + ' solid ' + theme.accentColor,
      'color': theme.primaryColor
    };

    theme.textInputBlurStyle = {
      // 'background': theme.primaryColorOp1,
      'border-top': thinBorder + ' solid ' + theme.accentColorOp1,
      'border-bottom': thinBorder + ' solid ' + theme.accentColorOp1,
      'border-left': fatBorder + ' solid ' + theme.accentColorOp4,
      'border-right': thinBorder + ' solid ' + theme.accentColorOp1,
      'color': theme.primaryColor
    };

    theme.newTxTextInputFocusStyle = {
      // 'background': theme.newTxColorOp1,
      'border-top': thinBorder + ' solid ' + theme.newTxColor,
      'border-bottom': thinBorder + ' solid ' + theme.newTxColor,
      'border-left': fatBorder + ' solid ' + theme.newTxColor,
      'border-right': thinBorder + ' solid ' + theme.newTxColor,
      'color': theme.newTxColor
    };

    theme.newTxTextInputBlurStyle = {
      // 'background': theme.newTxColorOp1,
      'border-top': thinBorder + ' solid ' + theme.newTxColorOp1,
      'border-bottom': thinBorder + ' solid ' + theme.newTxColorOp1,
      'border-left': fatBorder + ' solid ' + theme.newTxColorOp4,
      'border-right': thinBorder + ' solid ' + theme.newTxColorOp1,
      'color': theme.newTxColor
    };

    theme.selectOverlayStyle = {
      'background':
      'linear-gradient(' + 'rgba(' +
      theme.primaryColorRgb.r + ',' +
      theme.primaryColorRgb.g + ',' +
      theme.primaryColorRgb.b + ',' +
      theme.op2 + ')' + ',' + 'rgba(' +
      theme.primaryColorRgb.r + ',' +
      theme.primaryColorRgb.g + ',' +
      theme.primaryColorRgb.b + ',' +
      theme.op2 + ')' + '), ' +
      theme.secondaryColor,
      'color': theme.primaryColor
    };

    theme.selectArrowStyle = {
      'border-color': theme.primaryColor + ' transparent transparent transparent'

    };
    theme.newTxSelectArrowStyle = {
      'border-color': theme.newTxColor + ' transparent transparent transparent'
    };

    theme.newTransactionTextStyle = this.luma(themeSource.primaryColor) > this.luma(themeSource.secondaryColor) ?
      {
        'color': themeSource.secondaryColor,
        'border-color': themeSource.secondaryColor
      } :
      {
        'color': themeSource.primaryColor,
        'border-color': themeSource.primaryColor
      };

    theme.buttonStyle = {
      'color': themeSource.secondaryColor,
      'background-color': themeSource.primaryColor
    };
    theme.secondaryButtonStyle = {
      'color': themeSource.primaryColor,
      'background-color': themeSource.secondaryColor
    };
    theme.accentButtonStyle = {
      'color': themeSource.accentColor,
      'background-color': themeSource.accentColor
    };

    theme.maxButtonOffStyle = {
      'color': theme.newTxColor,
      'border-color': theme.newTxColor,
      'opacity': theme.newTxOp1
    };
    theme.maxButtonOnStyle = {
      'color': themeSource.processingColor,
      'border-color': theme.newTxColor,
      'background': theme.newTxColor
    };

    theme.primaryBorderStyle = {
      'border-color': theme.primaryColor
    };
    theme.primaryBorderStyleOp1 = {
      'border-color': theme.primaryColorOp1
    };
    theme.primaryBorderStyleOp2 = {
      'border-color': theme.primaryColorOp2
    };
    theme.primaryBorderStyleOp3 = {
      'border-color': theme.primaryColorOp3
    };
    theme.primaryBorderStyleOp4 = {
      'border-color': theme.primaryColorOp4
    };

    theme.secondaryBorderStyle = {
      'border-color': theme.secondaryColor
    };
    theme.secondaryBorderStyleOp1 = {
      'border-color': theme.secondaryColorOp1
    };
    theme.secondaryBorderStyleOp2 = {
      'border-color': theme.secondaryColorOp2
    };
    theme.secondaryBorderStyleOp3 = {
      'border-color': theme.secondaryColorOp3
    };
    theme.secondaryBorderStyleOp4 = {
      'border-color': theme.secondaryColorOp4
    };

    theme.accentBorderStyle = {
      'border-color': theme.accentColor
    };
    theme.accentBorderStyleOp1 = {
      'border-color': theme.accentColorOp1
    };
    theme.accentBorderStyleOp2 = {
      'border-color': theme.accentColorOp2
    };
    theme.accentBorderStyleOp3 = {
      'border-color': theme.accentColorOp3
    };
    theme.accentBorderStyleOp4 = {
      'border-color': theme.accentColorOp4
    };

    theme.newTxBorderStyle = {
      'border-color': theme.newTxColor
    };
    theme.newTxBorderStyleOp1 = {
      'border-color': theme.newTxColorOp1
    };
    theme.newTxBorderStyleOp2 = {
      'border-color': theme.newTxColorOp2
    };
    theme.newTxBorderStyleOp3 = {
      'border-color': theme.newTxColorOp3
    };
    theme.newTxBorderStyleOp4 = {
      'border-color': theme.newTxColorOp4
    };

    theme.toggleButtonOffStyle = {
      'border': '1px solid ' + 'rgba(' +
      primaryColorRgb.r + ',' +
      primaryColorRgb.g + ',' +
      primaryColorRgb.b + ',' +
      theme.op4 + ')',
      'background': 'transparent',
      'color': 'rgba(' +
               primaryColorRgb.r + ',' +
               primaryColorRgb.g + ',' +
               primaryColorRgb.b + ',' +
               theme.op4 + ')'
    };

    theme.toggleButtonOnStyle = {
      'border': 'none',
      'background': 'rgba(' +
                    primaryColorRgb.r + ',' +
                    primaryColorRgb.g + ',' +
                    primaryColorRgb.b + ',' +
                    theme.op4 + ')',
      'color': theme.secondaryColor
    };

    theme.toggleSwitchOffStyle = {
      'background-color': 'rgba(' +
      primaryColorRgb.r + ',' +
      primaryColorRgb.g + ',' +
      primaryColorRgb.b + ',' +
      theme.op2 + ')',
    };

    theme.toggleSwitchOnStyle = theme.primaryColor === theme.accentColor ? {
      'background-color': 'rgba(' +
      accentColorRgb.r + ',' +
      accentColorRgb.g + ',' +
      accentColorRgb.b + ',' +
      theme.op4 + ')',
    } : {
      'background-color': 'rgba(' +
      accentColorRgb.r + ',' +
      accentColorRgb.g + ',' +
      accentColorRgb.b + ',' +
      theme.op4 + ')',
    };

    theme.selectStyle = {
      'background-color': 'rgba(' +
      primaryColorRgb.r + ',' +
      primaryColorRgb.g + ',' +
      primaryColorRgb.b + ',' +
      theme.op2 + ')',
    };

    theme.processingBackgroundStyle = {
      'background-color': themeSource.processingColor
    };

    theme.processingColor = themeSource.processingColor;

    theme.receiveColor = themeSource.receiveColor;

    theme.sendColor = themeSource.sendColor;

    return theme;
  }

  public updateSVGs(theme) {
    // This is part of a hack-ey (but seemingly necessary) solution to dynamically change the color of
    // SVG images/icons to match the theme. It is used alongside ng-inline-svg, and the .primary-svg or .secondary-svg
    // class must be placed on the element with the [inlineSVG] directive, to indicate its color in relation to the theme.
    // I also manually edit the .svg files so that their height and width attributes are 100%, so they can be sized
    // by their parent element.
    if (theme.primaryColor) {
      const primaryColorPaths = document.querySelectorAll('.primary-svg svg path');
      for (let i = 0; i < primaryColorPaths.length; i++) {
        primaryColorPaths[i].setAttribute('fill', theme.primaryColor);
        // primaryColorPaths[i].style.fill = primaryColor;
      }
      const primaryColorPolygons = document.querySelectorAll('.primary-svg svg polygon');
      for (let i = 0; i < primaryColorPolygons.length; i++) {
        primaryColorPolygons[i].setAttribute('fill', theme.primaryColor);
        // primaryColorPolygons[i].style.fill = primaryColor;
      }
    }

    if (theme.secondaryColor) {
      const secondaryColorPaths = document.querySelectorAll('.secondary-svg svg path');
      for (let i = 0; i < secondaryColorPaths.length; i++) {
        secondaryColorPaths[i].setAttribute('fill', theme.secondaryColor);
        // secondaryColorPaths[i].style.fill = secondaryColor;
      }
      const secondaryColorPolygons = document.querySelectorAll('.secondary-svg svg polygon');
      for (let i = 0; i < secondaryColorPolygons.length; i++) {
        secondaryColorPolygons[i].setAttribute('fill', theme.secondaryColor);
        // secondaryColorPolygons[i].style.fill = secondaryColor;
      }
    }

    if (theme.accentColor) {
      const accentColorPaths = document.querySelectorAll('.accent-svg svg path');
      for (let i = 0; i < accentColorPaths.length; i++) {
        accentColorPaths[i].setAttribute('fill', theme.accentColor);
        // secondaryColorPaths[i].style.fill = secondaryColor;
      }
      const accentColorPolygons = document.querySelectorAll('.accent-svg svg polygon');
      for (let i = 0; i < accentColorPolygons.length; i++) {
        accentColorPolygons[i].setAttribute('fill', theme.accentColor);
        // secondaryColorPolygons[i].style.fill = secondaryColor;
      }
    }
  }

  public getRandomTheme() {
    const currentThemeIndex = this.themes.findIndex(t => t.name === this.theme.value.name);
    let newThemeIndex = currentThemeIndex;
    while (newThemeIndex == currentThemeIndex) {
      newThemeIndex = Math.floor(Math.random() * (this.themes.length - 1));
    }
    return this.themes[newThemeIndex];
  }

  private findThemeByName(themeName: string) {
    let theme = this.themes.find(t => t.name === themeName);
    if (isNullOrUndefined(theme)) {
      theme = this.themes.find(t => t.name === 'Default');
    }
    return theme;
  }

  public setTheme(themeName: string) {
    const theme = this.findThemeByName(themeName);
    this.preferencesService.setTheme(theme);
  }
}
