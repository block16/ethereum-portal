import { Injectable } from '@angular/core';
import {KeyManagerService} from "./key.manager.interface";
import {Observable} from "rxjs/Observable";
import {EthereumTransaction} from '../../shared/model/ethereum-transaction';
import {isArray, isNullOrUndefined} from "util";
import {NotificationService} from "../notification.service";

declare const Web3;
declare const web3;

@Injectable()
export class MetamaskService implements KeyManagerService {

  private currentAccount: string;
  private accountInterval;
  public providerDetected: boolean;
  public web3js: any;

  constructor(
    private notificationService: NotificationService
  ) { }

  /**
   * Detects to see if a user changes metamask accounts and refreshes the page
   */
  private detectSwitch() {
    this.accountInterval = setInterval(() => {
      this.web3js.eth.getAccounts().then((accounts) => {
        if (isArray(accounts) && accounts.length > 0) {
          if (this.currentAccount !== accounts[0]) {
            // Refresh page if metamask account changes
            location.reload();
          }
        } else if (!isArray(accounts) && !isNullOrUndefined(accounts)) {
          if (this.currentAccount !== [accounts][0]) {
            // Refresh page if metamask account changes
            location.reload();
          }
        }
      });
    }, 500);
  }

  public getEthereumAddresses(): Observable<string[]> {
    if (typeof web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3js = new Web3(web3.currentProvider);
      this.providerDetected = true;
      return Observable.create((observer) => {
        this.web3js.eth.getAccounts().then((accounts) => {
          if (isArray(accounts) && accounts.length > 0) {
            observer.next(accounts.map(a => a.substring(2)));
            // hackey but to detect account switching
            this.currentAccount = accounts[0];
            this.detectSwitch();
          } else if (!isArray(accounts) && !isNullOrUndefined(accounts)) {
            observer.next([accounts].map(a => a.substring(2)));
            // hackey but to detect account switching
            this.currentAccount = [accounts][0];
            this.detectSwitch();
          } else {
            this.notificationService.error("Please unlock metamask and select this option again.", "MetaMask");
            observer.error("Could not get accounts from provider");
          }
          observer.complete();
        }, err => {
          observer.error(err);
          observer.complete();
        });
      });
    } else {
      this.notificationService.error("MetaMask", "Please install metamask before using this option.");
    }
  }

  /**
   * Identity sign TX function as it will have to be approved
   * @param {EthereumTransaction} transaction
   * @returns {Observable<EthereumTransaction>}
   */
  signTransaction(transaction: EthereumTransaction): Observable<EthereumTransaction> {
    return Observable.create(observer => {
      observer.next(transaction);
      observer.complete();
    });
  }

  /**
   *
   * @param {EthereumTransaction} transaction
   * @returns {Observable<string>}
   */
  approveAndSend(transaction: EthereumTransaction): Observable<string> {
    return Observable.create(observer => {
      const unsigned = transaction.getUnsignedObject();
      this.web3js.eth.sendTransaction(unsigned, (err, txHash) => {
        if (!err) {
          observer.next(txHash);
        } else {
          observer.error(err);
        }
        observer.complete();
      });
    });
  }

  resetState() { }
}
