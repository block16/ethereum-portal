import {Injectable, NgZone} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {KeyManagerService} from './key.manager.interface';
import {EthereumTransaction} from '../../shared/model/ethereum-transaction';
import {Web3Service} from "../web3.service";
import {Buffer} from "buffer";
import {NotificationService} from "../notification.service";
import TrezorConnect from 'trezor-connect';

declare const ethereumjs;

export declare enum InputScriptType {
  SPENDADDRESS = 0,    // standard p2pkh address
  SPENDMULTISIG = 1,   // p2sh multisig address
  EXTERNAL = 2,        // reserved for external inputs (coinjoin)
  SPENDWITNESS = 3,    // native segwit
  SPENDP2SHWITNESS = 4 // segwit over p2sh (backward compatible)
}

export declare enum OutputScriptType {
  PAYTOADDRESS = 0,    // used for all addresses (bitcoin, p2sh, witness)
  PAYTOSCRIPTHASH = 1, // p2sh address (deprecated; use PAYTOADDRESS)
  PAYTOMULTISIG = 2,   // only for change output
  PAYTOOPRETURN = 3,   // op_return
  PAYTOWITNESS = 4,    // only for change output
  PAYTOP2SHWITNESS = 5 // only for change output
}

export declare interface HDNodeType {
  depth: number;
  fingerprint: number;
  child_num: number;
  chain_code: string;
  private_key: string;
  public_key: string;
}

export declare interface HDNodePathType {
  node: HDNodeType;    // BIP-32 node in deserialized form
  address_n: number[]; // BIP-32 path to derive the key from node
}

export declare interface MultisigRedeemScriptType {
  pubkeys: HDNodePathType[]; // pubkeys from multisig address (sorted lexicographically)
  signatures: string[];      // existing signatures for partially signed input
  m: number;                  // "m" from n, how many valid signatures is necessary for spending
}

export declare interface TxInputType {
  address_n: number[];
  prev_hash: string;
  prev_index: number;
  script_sig: string;
  sequence: number;
  script_type: InputScriptType;
  multisig: MultisigRedeemScriptType;
  amount: number;
}


export declare interface TxOutputType {
  address: string;
  address_n: number[];
  amount: number;
  script_type: OutputScriptType;
  multisig: MultisigRedeemScriptType;
  op_return_data: string;
}

export declare interface Recipient {
  address: string;
  amount: number;
}

@Injectable()
export class TrezorConnectService implements KeyManagerService {
  private _trezorConnect: TrezorConnect;

  constructor(
    private zone: NgZone,
    private web3Service: Web3Service,
    private notificationService: NotificationService
  ) {
    this._trezorConnect = TrezorConnect;
  }

  /**
   *
   * @param hosticon
   * @param challenge_hidden
   * @param challenge_visual
   * @returns {Observable<T>}
   */
  public requestLogin(hosticon: string, challenge_hidden: string, challenge_visual: string): Observable<any> {
    const subject = new Subject();

    this._trezorConnect.requestLogin(hosticon, challenge_hidden, challenge_visual, result => {
      if (result.success) {
        subject.next(result);
      } else {
        subject.error(result);
      }
    });

    return subject.asObservable();
  }

  private removePrefix(input: string) {
    return input.startsWith("0x") ? input.substr(2) : input;
  }

  public getEthereumAddresses(): Observable<string[]> {
    return Observable.create(observer => {
      try {
        const params = {
          path: "m/44'/60'/0'/0/0"
        };
        this._trezorConnect.ethereumGetAddress(params).then(result => {
          if (result.success) {
            observer.next([result.payload.address]);
          } else {
            this.errorSwitch(result.error);
            observer.error(result.error);
          }
          observer.complete();
        });
      } catch (e) {
        this.errorSwitch(e.message);
        observer.error(e.message);
        observer.complete();
      }
    });
  }

  signTransaction(transaction: EthereumTransaction): Observable<EthereumTransaction> {
    return null;
  }

  approveAndSend(transaction: EthereumTransaction): Observable<string> {
    return Observable.create(observer => {
      const params = {
        'path': "m/44'/60'/0'/0/0",
        'transaction': {
            to: this.removePrefix(transaction.toAddress),
            value: this.removePrefix(transaction.value),
            data: this.removePrefix(transaction.data),
            chainId: 1,
            nonce: this.removePrefix(transaction.nonce),
            gasLimit: this.removePrefix(transaction.gasLimit),
            gasPrice: this.removePrefix(transaction.gasPrice)
          }
      };

      this._trezorConnect.ethereumSignTransaction(params).then((result) => {
        if (result.success) {
          const rawTx = {
            nonce: transaction.nonce,
            gasPrice: transaction.gasPrice,
            gasLimit: transaction.gasLimit,
            to: transaction.toAddress,
            value: transaction.value,
            data: transaction.data,
            r: result.payload.r,
            s: result.payload.s,
            v: result.payload.v
          };

          const tx = new ethereumjs.Tx(rawTx);

          if (!tx.verifySignature()) {
            this.notificationService.error('Invalid signature produced from Trezor device.', 'Trezor');
            observer.error('Invalid signature');
            observer.complete();
          } else {
            transaction.signature = '0x' + tx.serialize().toString('hex');
            this.web3Service.sendRawTransaction(transaction).subscribe(txHash => {
              observer.next(txHash);
              observer.complete();
            }, (err) => {
              this.errorSwitch(err);
              observer.error(err);
              observer.complete();
            });
          }
        }
        // Error from this function propagates into a full blown error call. Have to use subscribe to only get once.
      }, e => {
        this.errorSwitch(e);
        observer.error(e);
        observer.complete();
      });
    });
  }

  resetState() {
  }

  errorSwitch(mesg) {
    /* if (err.message.includes('gas * price + value')) {
      this.notificationService.error('Invalid transaction, gas * price + value is greater than wallet balance.', 'Trezor');
    } */
    if (mesg === "Window closed") {
      this.notificationService.error(
        'Window closed, please re-initiate action.',
        'Trezor'
      );
    } else if (mesg === "Window blocked") {
      this.notificationService.error(
        "Please allow the site to create pop-ups in order to allow the Trezor to send transactions.",
        "Trezor Pop-up Blocked"
      );
    } else if (mesg === "Already waiting for a response") {
      this.notificationService.error(
        "Already waiting on response from device. Check popup window.",
        "Trezor"
      );
    } else {
      this.notificationService.error(
        mesg,
        "Trezor"
      );
    }
  }
}
