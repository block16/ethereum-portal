import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';
import TransportU2F from '@ledgerhq/hw-transport-u2f';
import LedgerEth from '@ledgerhq/hw-app-eth';
import {KeyManagerService} from "./key.manager.interface";
import {EthereumTransaction} from "../../shared/model/ethereum-transaction";
import {Web3Service} from "../web3.service";
import {NotificationService} from "../notification.service";
import {Buffer} from "buffer";

declare var ethereumjs;

@Injectable()
export class LedgerService implements KeyManagerService {
  private ledgerEth: LedgerEth;

  private address: string;
  private path: string;
  private getChaincode = false;
  private displayAddress = false;

  constructor(
    private web3Service: Web3Service,
    private notificationService: NotificationService
  ) {
    this.resetState();
    this.path = "44'/60'/0'/0";
  }

  approveAndSend(transaction: EthereumTransaction): Observable<string> {
    return Observable.create(o => {

      const unsigned = new ethereumjs.T({
        nonce: transaction.nonce,
        gasPrice: transaction.gasPrice,
        gasLimit: transaction.gasLimit,
        to: transaction.toAddress,
        value: transaction.value,
        data: transaction.data,
        chainId: 1
      });

      unsigned.raw[6] = Buffer.from([1]); // v
      unsigned.raw[7] = Buffer.from([]);  // r
      unsigned.raw[8] = Buffer.from([]);  // s

      const serialized = unsigned.serialize().toString('hex');

      this.ledgerEth.signTransaction(this.path, serialized).then((r: any) => {

        unsigned.v = Buffer.from(r.v, 'hex');
        unsigned.r = Buffer.from(r.r, 'hex');
        unsigned.s = Buffer.from(r.s, 'hex');

        if (!unsigned.verifySignature()) {
          console.log("Invalid signature");
          this.notificationService.error("Invalid signature returned", "Ledger");
          throw new Error("Invalid signature returned from ledger");
        }

        transaction.signature = ethereumjs.Util.addHexPrefix(unsigned.serialize().toString('hex'));
        this.web3Service.sendRawTransaction(transaction).subscribe(txHash => {
          o.next(txHash);
          o.complete();
        }, (err) => {
          o.error(err);
          o.complete();
        });
      }, err => {
        o.error(err);
        o.complete();
      });
    });
  }

  getEthereumAddresses(): Observable<string[]> {
    return Observable.create(observer => {
      if (isNullOrUndefined(this.ledgerEth)) {
        TransportU2F.create().then((t) => {
          this.ledgerEth = new LedgerEth(t);
          this.ledgerEth.getAddress(this.path, this.displayAddress, this.getChaincode).then(a => {
            this.address = a.address;
            observer.next([a.address]);
            observer.complete();
          });
        }, err => this.errorSwitch(err));
      } else {
        this.ledgerEth.getAddress(this.path, this.displayAddress, this.getChaincode).then(a => {
          this.address = a.address;
          observer.next([a.address]);
          observer.complete();
        }, err => this.errorSwitch(err));
      }
    });
  }

  private errorSwitch(err) {
    if (err.message.includes('U2F not supported')) {
      this.notificationService.error('Browser doesn\'t support U2F. Consider using a browser that does before selecting this option.', 'Ledger');
    } else if (err.id.includes('TransportLocked') || err.id === 'U2F_5') {
      this.notificationService.error('Please unlock your ledger device before selecting this option.', 'Ledger');
    } else if (err.statusCode === 27013) {
      this.notificationService.error('Transaction cancelled by user.', 'Ledger');
    }
  }

  resetState() {
    this.address = '';
  }

  signTransaction(transaction: EthereumTransaction): Observable<EthereumTransaction> {
    return null;
  }

  private bip44IndexToBip32Path = (index: number) => `44'/60'/0'/0/${index}`;
}
