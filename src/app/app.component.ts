import {Component, OnInit} from '@angular/core';
import {UserPreferencesService} from "./core/user-preferences.service";
import {BigNumber} from 'bignumber.js';
import {ThemeService} from "./core/theme.service";
import {UserPreferences} from "./shared/model/user-preferences";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private themeSet = false;

  constructor(
    private userPreferencesService: UserPreferencesService,
    private themeService: ThemeService
  ) {
    BigNumber.config({ EXPONENTIAL_AT: [-40, 40], DECIMAL_PLACES: 40 });
  }

  ngOnInit(): void {
    this.userPreferencesService.userPreferences.subscribe((userPref: UserPreferences) => {
      if (userPref.randomTheme && !this.themeSet) {
        this.themeSet = true;
        const theme = this.themeService.getRandomTheme();
        this.userPreferencesService.setTheme(theme);
      }
    });
  }
}
