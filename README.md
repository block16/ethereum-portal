# EthereumPortal
A simple opinionated web interface for interacting with Ethereum and Tokens.

If you enjoy this product, checkout our APIs: [article](https://medium.com/block-16/ethereum-block-transaction-info-via-aggregated-views-api-by-block-16-aa9b175da929), [docs](https://docs.block16.io/), [websocket example](https://docs.block16.io/example/index.html).

### Installing
After cloning the repo use the npm run command to install dependencies:
```
npm run fullInstall
```

### Running Locally

```
npm run start
```

Open a browser to `localhost:4200` to access the app. Changes to code will automatically be detected and rebuild the application locally. An internet connection is still required to access transaction information for the application to run.

### Building a Distribution

```
npm run build
```

The build will be output into the directory `dist/`. Open `dist/index.html` to view the webpage locally. An internet connection is still required to access transaction information.

### Contributing

See `CONTRIBUTING.md`


### Contact

If you have any questions or comments, please open an issue in this repo or send an email to `support@block16.io` .

![Block 16](https://block16.io/assets/img/assets_logo%20white.svg)

### License

Copyright (c) 2018 Block 16 Advisory

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


